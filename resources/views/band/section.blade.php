<!-- BEGIN THE BAND SECTION -->
<section id="theband">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h1 class="section-title">O nama</h1>
                <p class="section-desc">
                    {{$band->description}}
                    <a href="" class="btn btn-dark btn-icon open-overlay" data-overlay-id="theband-overlay"><i class="icon-file-text-alt"></i><span>Više o nama</span></a>
                </p>
            </div>
            <img src="img/band.png" alt="" id="bandImage" class="col-sm-8"  />
        </div>
    </div>
    <div id="bottom-theband" class="bg-color2"></div>
</section>
<!-- END THE BAND SECTION -->
@include('band.overlay')