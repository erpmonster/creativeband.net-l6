<!-- Begin About Band Page Overlay -->
<div class="page-overlay" id="theband-overlay">
    <i class="icon-remove-circle close-overlay"></i>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="center">Mi smo Creative Band!</h1>
                <div class="row">
                <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/marija.png" alt="" />
                        <h3>Marija Mitrović<span> - Vokal</span></h3>
                    </div>
                    <p>
                        Rodjena 80-ih ☺ Bavim se muzikom od svoje pete godine.
                        Učestvovala na raznim takmičenjima i igrala u folkloru.
                        Pevala u dva benda i nakon toga pridružila se Creative band-u, i ostala 14 godina...
                        Nadam se da ću poslednji nastup završiti sa istim..i to za nekih 20-30 godina.
                        I eto to vam je samo delić mene</p>
                </div>
                <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/jelena pajic_372.jpg" alt="" />
                        <h3>Jelena Pajić<span> - Vokal</span></h3>
                    </div>
                    <p>
                    S mikrofonom se druži maltene od kad je progovorila. Kao prateći vokal nastupala je sa većinom zvezda sa naše estrade i bila neizostavni deo ekipe na koncertima, festivalima pa i na najprestižnijem evropskom festivalu Evrovizija.
Napokon pronašla sebe kao lead vokal u Creative bendu.</p>
                </div>
                <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/boki.png" alt="" />
                        <h3>Bojan Petrović<span> - Vokal</span></h3>
                    </div>
                    <p>Muški vokal u porodici "Creative band", rodjen u Lebanu 6. maja davne 1978...
                        Od samog početka u Creative band-u gde se konačno pronalazi i zaljubljuje u muziku.
                        Prema svim izvodjačima se odnosi sa poštovanjem, a preferira tvrdji zvuk i "kvalitetnu" narodnu muziku, i naravno CREATIVE BAND.</p>
                </div>
                

            </div>
            <div class="row">
            <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/bata.png" alt="" />
                        <h3>Bratislav Milenković<span> - Bubnjevi</span></h3>
                    </div>
                    <p>Idejni vodja i osnivač benda.
                        Posle više godina sviranja u raznim bendovima
                        sa svojim prijateljima osniva Creative bend sa željom
                        da zajedno odsviraju i koncert za 30 godina postojanja.
                        Veliki zaljubljenik u motocikle, slobodno vreme
                        provodi putujući motorom po svetu.</p>
                </div>
                <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/member04.png" alt="" />
                        <h3>Zulović Edin<span> - Klavijature</span></h3>
                    </div>
                    <p>Rodjen 1975. Muzička srednja vojna škola učvršćava moju vezu sa muzikom i u toku školovanja otkrivam novi svet koji će obeležiti dalji život.
                        Pored benda radim i kao studijski muzičar i saradjujem sa raznim izvodjačima...</p>
                </div>
                <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/member05.png" alt="" />
                        <h3>Matić Andrija<span> - Gitara</span></h3>
                    </div>
                    <p>Rodjen 1981. godine u Paraćinu. Dodir sa muzikom imam jos od klinačkih dana. Slušajući uz ćaleta Rock'N'Roll počeo sam da maštam o bendu, gitari i toj nekoj lepšoj strani života. Danas, svirajući u Creative bend-u, najveći deo tih snova je postao stvarnost.</p>
                </div>

            </div>
            <div class="row">
            <div class="band-elem col-sm-4">
                    <div class="band-elem-img">
                        <img src="img/members/member06.png" alt="" />
                        <h3>Stojanović Bojan<span> - Bas</span></h3>
                    </div>
                    <p>Basista sam Creative bend-a od osnivanja. Bavim se muzikom od šeste godine. Pobornik sam tvrdjeg zvuka kao i ostali članovi i eto nas. Pored muzike se još bavim i slikarstvom. Verujem u Creative bend i znam da naše vreme tek dolazi.</p>
                </div>
            </div>

                <h1 class="center">Naši albumi</h1>

                <div class="album">
                    <div class="album-img col-sm-4">
                        <div class="album-img-wrap">
                            <img src="img/albums/ironija305.jpg" alt="" />
                        </div>
                    </div>
                    <div class="album-info col-sm-8">
                        <h3 class="color">Ironija<br/><span>2008</span></h3>
                        <p>Drugi album Creative Banda ugledao svetlost dana decembra 2008! Album je dobrim delom skroz u fazonu benda sa malo komercijale. Iskreno se nadamo da vam se album svidja jer smo zaista kompletno uložili sebe da to ispadne što bolje. Naslovnu numeru nosi pesma "Ironija" a koju je bend objavio kao singl u junu 2008.</p>
                        <ol>
                            <li><span>Ironija</span> <br><span class="time">Tekst: Maja Leković / Muzika: Nena Leković / Aranžman: Alek Aleksov</span></li>
                            <li class="darker"><span>Mala ajkula</span><br><span class="time">Tekst: Dragiša Baša / Muzika: Dragiša Baša / Aranžman: Vuk Zirojević</span></li>
                            <li class="darker"><span>Brodolom</span><br><span class="time">Tekst: Bratislav Milenković / Muzika: Creative Band / Aranžman: Alek Aleksov</span></li>
                            <li><span>Prečica za bol</span><br><span class="time">Tekst: Vesna Zakonović / Muzika: Saša i Suzana Dinić / Aranžman: Alek Aleksov</span></li>
                            <li><span>Dvoličan</span><br><span class="time">Tekst: Zoran Leković / Muzika: Nena Leković / Aranžman: Alek Aleksov</span></li>
                            <li class="darker"><span>Svake noći neka druga pored mene</span><br><span class="time">Tekst: Dragiša Baša / Muzika: Dragiša Baša / Aranžman: Vuk Zirojević</span></li>
                            <li class="darker"><span>Zajedno</span><br><span class="time">Tekst: Maja i Zoran Leković / Muzika: Nena Leković / Aranžman: Alek Aleksov</span></li>
                            <li><span>Ne diraj me</span><br><span class="time">Tekst: Zoran Leković / Muzika: Nena Leković / Aranžman: Alek Aleksov</span></li>
                        </ol>
                        <p>Prateći vokali:
                            01 - Marija Mitrović
                            02, 04, 06 - Ivana Selakov
                            05 - Leontina Vukomanović
                            07 - Ivana Čabraja i Jadranka Krištof</p>
                    </div>
                </div>

                <div class="album">
                    <div class="album-img col-sm-4">
                        <div class="album-img-wrap">
                            <img src="http://placehold.it/305x305" alt="" />
                        </div>
                    </div>
                    <div class="album-info col-sm-8">
                        <h3 class="color">U vašim rukama<br/><span>2006</span></h3>
                        <p>"U vašim rukama" je prvi studijski album Creative banda na kome se nalaze pesme "Crni somot" i "Vreme za kraj" sa kojima je bend nastupao na Budvanskom festivalu i na festivalu u Vrnjačkoj banji. U nastavku možete videti tim ljudi koji je radio na albumu.</p>
                        <ol>
                            <li><span>Probaj me</span><br><span class="time">Tekst: Vesna Zakonović / Muzika: Vesna Zakonović / Aranžman: Srdjan Moby</span></li>
                            <li class="darker"><span>Vreme za kraj</span><br><span class="time">Tekst: Creative Band / Muzika: Vesna Zakonović / Aranžman: Creative Band</span></li>
                            <li class="darker"><span>U tvojim rukama</span><br><span class="time">Tekst: Ivica Vasić / Muzika: Ivica Vasić / Aranžman: Branko Rio</span></li>

                            <li><span>Odlazi sa njim</span><br><span class="time">Tekst: Ivica Vasić / Muzika: Ivica Vasić / Aranžman: Buzda .........................</span></li>

                            <li><span>Crni somot</span><br><span class="time">Tekst: Saša Dragić / Muzika: Saša Dragić / Aranžman: Alek Aleksov</span></li>
                            <li class="darker"><span>Zaboravi sve</span><br><span class="time">Tekst: Branko Rio / Muzika: Branko Rio / Aranžman: Branko Rio</span></li>
                            <li class="darker"><span>Tvoja malena</span><br><span class="time">Tekst: Ana Rančić / Muzika: Ana Rančić / Aranžman: I. Milosavljević</span></li>
                            <li><span>Još sam tu</span><br><span class="time">Tekst: Ivica Vasić / Muzika: Ivica Vasić / Aranžman: Buzda .........................</span></li>
                            <li><span>Ostani</span><br><span class="time">Tekst: Ivica Vasić / Muzika: Ivica Vasić / Aranžman: Branko Rio</span></li>

                        </ol>
                    </div>
                    <div class="album-info col-sm-8 col-sm-offset-4">

                        <p>Prateći vokali:
                            01, 02 - Bojan Petrović, Vladan Ignjatović i Bratislav Milenković
                            02, 03 - Marija Mitrovic
                            03, 06, 09 - Ana Štajdohar
                            05 - Leonitna Vukomanović
                            07 - Ana Rančić </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End About Band Page Overlay -->
