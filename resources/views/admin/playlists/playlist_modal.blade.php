        <div class="modal fade in" id="playlistsForm" tabindex="-1" role="dialog" aria-hidden="false" style="display:none;">
            <div class="modal-dialog modal-lg">
                <form class="form-horizontal" id="frmAddPlaylist" action="{{ URL::to('admin/playlists') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add item</h4>
                        </div>
                        <div class="modal-body">
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">Title</label>
                                    <div class="col-md-9">
                                        <input id="title" name="title" type="text" placeholder="Title" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Artist</label>
                                    <div class="col-md-9">
                                        <input id="artist" name="artist" type="text" placeholder="Artist" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">mp3</label>
                                    <div class="col-md-9">
                                        <input id="mp3" class="filestyle" type="file" name="mp3" accept="audio/*" data-buttonText="&nbsp;&nbsp;Choose file" data-buttonName="btn-primary">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">oga</label>
                                    <div class="col-md-9">
                                        <input id="oga" class="filestyle" type="file" name="oga" accept="audio/*" data-buttonText="&nbsp;&nbsp;Choose file" data-buttonName="btn-primary">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Duration</label>
                                    <div class="col-md-9">
                                        <input id="duration" name="duration" type="text" placeholder="Duration" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">Cover</label>
                                    <div class="col-md-9">
                                        <input id="cover" class="filestyle" type="file" name="cover" accept="image/*" data-buttonText="&nbsp;&nbsp;Choose file" data-buttonName="btn-primary">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Order index</label>
                                    <div class="col-md-9">
                                        <input id="order_index" name="order_index" type="text" placeholder="Order index" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Active</label>
                                    <div class="col-md-9">
                                        <input id="active" name="active" type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn">Close</button>
                            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>