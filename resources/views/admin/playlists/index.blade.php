@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Playlist manager
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')
            <!--page level css -->
    <link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Playlist</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Playlist</li>
            <li class="active">Playlist</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="clock" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Playlist management
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-large" data-toggle="modal" data-href="#playlistsForm" href="#playlistsForm">Add item
                                        <i class="fa fa-plus"></i></a>
                                </div>
                            </div>

                            <hr>

                            @include('admin.playlists.playlist_modal')

                            <div>
                                <table class="table table-striped table-bordered table-hover dataTable" id="table" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1">Title</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Artist
										: activate to sort column ascending">Artist</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 mp3
										: activate to sort column ascending">mp3</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 oga
										: activate to sort column ascending">oga</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Duration
										: activate to sort column ascending" style="width: 45px">Duration</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Cover
										: activate to sort column ascending">Cover</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Order index
										: activate to sort column ascending" style="width: 80px">Order index</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Active
										: activate to sort column ascending" style="width: 40px">Active</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Actions
										: activate to sort column ascending" style="width: 80px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($playlists as $playlist)
                                        <tr>
                                            <td>{{{ $playlist->title }}}</td>
                                            <td>{{{ $playlist->artist }}}</td>
                                            <td>{{{ $playlist->mp3 }}}</td>
                                            <td>{{{ $playlist->oga }}}</td>
                                            <td>{{{ $playlist->duration }}}</td>
                                            <td>{{{ $playlist->cover }}}</td>
                                            <td>{{{ $playlist->order_index }}}</td>
                                            <td>{{{ $playlist->active }}}</td>
                                            <td>
                                                <a data-toggle="modal" data-href="#playlistsForm" href="#playlistsForm" data-id="{{{ $playlist->id }}}" role="dialog">
                                                    <i class="livicon" data-name="edit" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update item"></i>
                                                </a>

                                                <a id="btnDelete" href="#modal_delete_confirm" data-toggle="modal" data-href="#modal_delete_confirm" data-route="{{{ route($model.'.delete', $playlist->id) }}}">
                                                    <i class="livicon" data-name="user-remove" data-size="20" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete item"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @include('admin.layouts.modal_confirmation')

        <div class="cbloader"></div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/select2.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/modal/js/classie.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/modal/js/modalEffects.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/bootstrap-filestyle/src/bootstrap-filestyle.min.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/playlists.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/modal_delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>
@stop