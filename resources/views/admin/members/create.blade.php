@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Add band member
    @parent
@stop

@section('header_styles')
    <link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    @parent
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Band</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Band</li>
            <li class="active">Add member</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> <i class="livicon" data-name="users" data-size="16" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            Add new member
                        </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                    </div>
                    <div class="panel-body">

                        <!--main content-->
                        <div class="row">
                            <div class="col-md-12">

                                <form class="form-wizard form-horizontal" action="{{route('members.store') }}" method="POST" id="frmBandMember" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <fieldset>

                                        <div class="form-group">
                                            <label for="first_name" class="col-sm-2 control-label">First name *</label>
                                            <div class="col-sm-10">
                                                <input id="first_name" name="first_name" type="text" placeholder="First name" class="form-control required" value="{{ Input::old('first_name') }}" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="last_name" class="col-sm-2 control-label">Last name *</label>
                                            <div class="col-sm-10">
                                                <input id="last_name" name="last_name" type="text" placeholder="Last name" class="form-control required" value="{{ Input::old('last_name') }}" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="role" class="col-sm-2 control-label">Role *</label>
                                            <div class="col-sm-10">
                                                <input id="last_name" name="role" type="text" placeholder="Role" class="form-control required" value="{{ Input::old('role') }}" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea id="description" name="description" rows="5" placeholder="Description" class="form-control required">{{ Input::old('description') }}</textarea>
                                            </div>
                                        </div>
                                        <!-- Order index input -->
                                        <div class="form-group">
                                            <label for="order_index" class="col-sm-2 control-label">Order index *</label>
                                            <div class="col-sm-10">
                                                <input id="order_index" name="order_index" type="text" placeholder="Order index" class="form-control required" value="{{ Input::old('order_index') }}" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2" id="error_msg">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail img-file" data-trigger="fileinput">
                                                        <img data-src="holder.js/372x371" alt="member image">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail img-max" style="line-height: 10px;">
                                                    </div>

                                                    <div>
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input id="image" type="file" name="image" accept="image/*">
                                                        </span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <p>(*) Mandatory</p>
                                        <div class="form-group">
                                            <div class="col-md-12 text-right">
                                                <button type="submit" class="btn btn-responsive btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/band_member.js') }}"></script>
@stop
