@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Band members
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('josh/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
<link href="{{ asset('josh/assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Band</h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.html"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Bands</li>
        <li class="active">Band members</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-10">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="livicon" data-name="clock" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
						Band members management
					</h3>					
				</div>
				<div class="panel-body">
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="btn-group">
                                <a class="btn btn-primary btn-large" href="{{ URL::route('members.create') }}">Add member
                                    <i class="fa fa-plus"></i></a>
                            </div>
                        </div>

                        <hr>

                        <div class="panel">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="table" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1">First Name</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1">Last Name</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-label="
                                         Role
                                    : activate to sort column ascending">Role</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-label="
                                         Description
                                    : activate to sort column ascending">Description</th>
                                     <th class="sorting" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-label="
                                         Order index
                                    : activate to sort column ascending">Order index</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-label="
                                         Actions
                                    : activate to sort column ascending">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($members as $member)
                                    <tr>
                                        <td>{{{ $member->first_name }}}</td>
                                        <td>{{{ $member->last_name }}}</td>
                                        <td>{{{ $member->role }}}</td>
                                        <td>{{{ $member->description }}}</td>
                                        <td>{{{ $member->order_index }}}</td>
                                        <td>
                                            <a href="{{ route('members.edit', $member->id) }}">
                                                <i class="livicon" data-name="edit" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update member"></i>
                                            </a>

                                            <a id="btnDelete" href="#modal_delete_confirm" data-toggle="modal" data-href="#modal_delete_confirm" data-route="{{{ route($model.'.delete', $member->id) }}}">
                                                <i class="livicon" data-name="user-remove" data-size="20" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete member"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>

                @include('admin.layouts.modal_confirmation')

			</div>
		</div>
	</div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/select2.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/modal_delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>
@stop