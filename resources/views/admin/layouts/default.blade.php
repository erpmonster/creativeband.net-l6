<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
        | Creative Band Control Panel
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link href="{{ asset('josh/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ asset('josh/assets/vendors/font-awesome-4.2.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Lato -->
    <link href="{{ asset('josh/assets/css/latofonts.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('josh/assets/css/styles/black.css') }}" rel="stylesheet" type="text/css" id="colorscheme" />
    <link rel="stylesheet" href="{{ asset('josh/assets/css/panel.css') }}" />
    <link rel="stylesheet" href="{{ asset('josh/assets/css/metisMenu.css') }}" />
    
    <!-- end of global css -->
    <!--page level css-->
    @yield('header_styles')
    <!--end of page level css-->
</head>

<body class="skin-josh">
    @include('admin.layouts.partials.header')

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        @include('admin.layouts.partials.sidebar')

        <aside class="right-side">

            <!-- Notifications -->
            @include('notifications')
            
            <!-- Content -->
            @yield('content')
        </aside>
        <!-- right-side -->
    </div>

    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('josh/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
    @if (Request::is('admin/form_builder2') || Request::is('admin/gridmanager') || Request::is('admin/portlet_draggable'))
        <script src="{{ asset('josh/assets/vendors/form_builder1/js/jquery.ui.min.js') }}"></script>
    @endif
    <script src="{{ asset('josh/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!--livicons-->
    <script src="{{ asset('josh/assets/vendors/livicons/minified/raphael-min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('josh/assets/js/josh.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/metisMenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/holder-master/holder.min.js') }}"></script>
    <script src="{{ asset('josh/assets/js/common.js') }}"></script>
    <!-- end of global js -->
    <!-- begin page level js -->
    @yield('footer_scripts')
    <!-- end page level js -->
</body>
</html>