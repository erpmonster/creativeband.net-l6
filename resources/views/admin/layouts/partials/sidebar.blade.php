    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar purplebg">
            <div class="page-sidebar  sidebar-nav">

                <div class="clearfix"></div>
                <!-- BEGIN SIDEBAR MENU -->
                <ul id="menu" class="page-sidebar-menu">
                    <li {{ (Request::is('admin') ? 'class="active"' : '') }}>
                        <a href="{{ route('dashboard') }}">
                            <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>

                    <li {{ (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') }}>
                        <a href="#">
                            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C" data-loop="true"></i>
                            <span class="title">Users</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {{ (Request::is('admin/users') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/users') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Users
                                </a>
                            </li>
                            <li {{ (Request::is('admin/users/create') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/users/create') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Add New User
                                </a>
                            </li>
                            <li {{ ((Request::is('admin/users/*')) && !(Request::is('admin/users/create')) ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::route('users.show', Sentry::getUser()->id) }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    View Profile
                                </a>
                            </li>
                            <li {{ (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/deleted_users') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Deleted Users
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li {{ (Request::is('admin/bands') || Request::is('admin/members') ? 'class="active"' : '') }}>
                        <a href="#">
                            <i class="livicon" data-name="users" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                            <span class="title">Band</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {{ (Request::is('admin/bands') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/bands') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Info
                                </a>
                            </li>

							<li {{ ((Request::is('admin/members')) ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::route('members.index') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Band members
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li {{ (Request::is('admin/galleries') ? 'class="active"' : '') }} >
                        <a href="#">
                            <i class="livicon" data-name="image" data-size="18" data-c="#6CC66C" data-hc="#6CC66C" data-loop="true"></i>
                            <span class="title">Gallery</span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="sub-menu">

                            <li {{ (Request::is('admin/galleries') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/galleries') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Manager
                                </a>
                            </li>

                        </ul>

                    </li>

                    <li {{ (Request::is('admin/albums') || Request::is('admin/albumsongs*')  ? 'class="active"' : '') }} >
                        <a href="#">
                            <i class="livicon" data-name="albums" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                            <span class="title">Albums</span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="sub-menu">

                            <li {{ (Request::is('admin/albums') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/albums') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Albums
                                </a>
                            </li>

                            <li {{ (Request::is('admin/albumsongs*') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/albumsongs') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Album songs
                                </a>
                            </li>

                        </ul>

                    </li>

                    <li {{ (Request::is('admin/playlists') ? 'class="active"' : '') }} >
                        <a href="#">
                            <i class="livicon" data-name="star-full" data-size="18" data-c="#6CC66C" data-hc="#6CC66C" data-loop="true"></i>
                            <span class="title">Playlists</span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="sub-menu">

                            <li {{ (Request::is('admin/playlists') ? 'class="active" id="active"' : '') }}>
                                <a href="{{ URL::to('admin/playlists') }}">
                                    <i class="fa fa-angle-double-right"></i>
                                    Playlist
                                </a>
                            </li>

                        </ul>

                    </li>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </section>
    </aside>