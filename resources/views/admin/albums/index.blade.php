@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Albums manager
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')
            <!--page level css -->
    <link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Albums</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Albums</li>
            <li class="active">Albums</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="albums" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Albums management
                        </h3>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
                            {{ $errors->first('year', '<span class="help-block">:message</span>') }}
                        </div>

                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-large" data-toggle="modal" data-href="#albumsForm" href="#albumsForm">Add album
                                        <i class="fa fa-plus"></i></a>
                                </div>
                            </div>

                            <hr>

                            @include('admin.albums.album_modal')

                            <div>
                                <table class="table table-striped table-bordered table-hover dataTable" id="table" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1">Title</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Year
										: activate to sort column ascending">Year</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Description
										: activate to sort column ascending">Description</th>
                                        <th class="sorting" tabindex="0"  rowspan="1" colspan="1" aria-label="
											 Description_2
										: activate to sort column ascending">Description_2</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Actions
										: activate to sort column ascending">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($albums as $album)
                                        <tr>
                                            <td>{{{ $album->title }}}</td>
                                            <td>{{{ $album->year }}}</td>
                                            <td>{{{ $album->description }}}</td>
                                            <td>{{{ $album->description_2 }}}</td>
                                            <td>
                                                <a data-toggle="modal" data-href="#albumsForm" href="#albumsForm" data-id="{{{ $album->id }}}" role="dialog">
                                                    <i class="livicon" data-name="edit" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update album"></i>
                                                </a>

                                                <a id="btnDelete" href="#modal_delete_confirm" data-toggle="modal" data-href="#modal_delete_confirm" data-route="{{{ route($model.'.delete', $album->id) }}}">
                                                    <i class="livicon" data-name="user-remove" data-size="20" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete album"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @include('admin.layouts.modal_confirmation')

        <div class="cbloader"></div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/select2.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/albums.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/modal_delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop