        <div class="modal fade in" id="albumsForm" tabindex="-1" role="dialog" aria-hidden="false" style="display:none;">
            <div class="modal-dialog modal-lg">
                <form class="form-horizontal" id="frmAddAlbum" action="{{ URL::to('admin/albums') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add album</h4>
                        </div>
                        <div class="modal-body">
                            <fieldset>

                                <input type="hidden" name="album_id" id="album_id" value="">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">Title</label>
                                    <div class="col-md-9">
                                        <input id="title" name="title" type="text" placeholder="Album title" class="form-control" value=""></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Year</label>
                                    <div class="col-md-9">
                                        <input id="year" name="year" type="text" placeholder="Release year" class="form-control" value=""></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Description</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="description" name="description" placeholder="Enter description here..." rows="4" >
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Description 2</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" id="description_2" name="description_2" placeholder="Enter description here..." rows="4">
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Album image</label>
                                    <div class="col-md-9" id="error_msg">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-file" data-trigger="fileinput">
                                                <img data-src="holder.js/215x215?text=₪" alt="album image">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail album-image" style="line-height: 10px;">
                                            </div>

                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input id="image" type="file" name="image" accept="image/*">
                                                </span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn">Close</button>
                            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>