    <div class="modal fade" id="modal_crop_image" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="user_delete_confirm_title">@lang('galleries/action.upload.title')</h4>
                </div>
                <form action="{{{ URL::to('admin/addimage/galleries/'. $initGallery->id) }}}" method="post" id="frmUploadImage" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" id="img_x" name="img_x" />
                        <input type="hidden" id="img_y" name="img_y" />
                        <input type="hidden" id="img_w" name="img_w" />
                        <input type="hidden" id="img_h" name="img_h" />

                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <i class="fa fa-fw fa-info-circle"></i>
                                    @lang('galleries/action.upload.body')
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div id="error_msg">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail img-file" data-trigger="fileinput">
                                        <img data-src="holder.js/560x430?text=₪"  alt="..."></div>
                                    <div class="fileinput-preview fileinput-exists thumbnail crop-preview">
                                    </div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input id="image" type="file" name="image"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('galleries/action.upload.cancel')</button>
                        <button type="submit" id="submit" class="btn btn-danger disabled">@lang('galleries/action.upload.confirm')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>