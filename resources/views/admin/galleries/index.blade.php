@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Gallery manager
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')
            <!--page level css -->
    <link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('josh/assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('josh/assets/vendors/gallery/basic/source/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('josh/assets/vendors/gallery/basic/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('josh/assets/vendors/gallery/basic/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('josh/assets/vendors/imgcrop/css/jquery.Jcrop.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Gallery manager</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Gallery</li>
            <li class="active">Gallery manager</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="image" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Galleries
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="portlet-body">

                            {{-- Galleries panel --}}

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="input-group bootstrap-touchspin input-group-sm">
                                        <input id="galleryname" name="galleryname" type="text" value="" class="input-sm form-control" placeholder="Gallery name">
                                        <span class="input-group-addon bootstrap-touchspin-postfix btn btn-default disabled" id="btnNewGallery">New gallery</span>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="pull-right">
                                        <div class="col-xs-12" id="pnlGalleryButtons">

                                            <div class="form-inline">
                                                @foreach ($galleries as $gallery)

                                                    <div class="input-group mr-15">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-primary btn-xs" type="button" id="filter-{{{ $gallery->name }}}" data-id="{{{ $gallery->id }}}" title="Click to select gallery">{{{ $gallery->name }}}</button>
                                                            <button type="button" id="btnActivate-{{{ $gallery->id }}}" data-id="{{{ $gallery->id }}}" data-active="{{{ $gallery->active }}}" title="Gallery activation" class="btn color-primary-plus btn-xs">
                                                                @if ($gallery->active == 1)
                                                                    <i class="fa fa-fw fa-check icon-white"></i>
                                                                @else
                                                                    <i class="fa fa-fw fa-times-circle icon-white"></i>
                                                                @endif

                                                            </button>

                                                            <a class="btn btn-default btn-xs" id="btnDeleteGallery-{{{ $gallery->id }}}" data-toggle="modal" href="#modal_delete_confirm" data-href="#modal_delete_confirm" data-route="{{{ route($model.'.delete', $gallery->id) }}}" title="Delete gallery">
                                                                <i class="fa fa-fw fa-times"></i>
                                                            </a>
                                                        </div>
                                                        <input id="order_index-{{{ $gallery->id }}}" data-id="{{{ $gallery->id  }}}" type="text" class="input-xs text-center" title="Gallery order index.&#13;Press enter to save." value="{{{ $gallery->order_index }}}">
                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            {{-- upload panel --}}
                            <div class="row form-group">
                                @if (!empty($initGallery))
                                    <div id="galleryCaption" class="col-md-9">
                                        <span><h5><strong>{{{ $initGallery->name }}}</strong></h5></span>
                                        <input type="hidden" id="gallery_id" value="{{{ $initGallery->id }}}">
                                    </div>
                                    <div class="col-md-3">
                                        <div id="pnlUpload" class="hidden">
                                            <a class="btn btn-default btn-large pull-right" data-toggle="modal" data-href="#modal_crop_image" href="#modal_crop_image">Select image
                                            <i class="fa fa-picture-o"></i></a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            {{-- Images panel --}}
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach ($images as $image)
                                        <div class="col-lg-2 col-md-3 col-xs-6 col-sm-3 gallery-image">
                                            <a class="fancybox" href="{{ asset($image->image) }}" data-fancybox-group="gallery" title="{{ $image->image }}">
                                                <img src="{{{ asset($image->image) }}}" class="img-responsive gallery-style" alt="{{ $image->image }}">
                                            </a>

                                            <div class="pull-right mt-5">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="btnDeleteImage-{{{ $image->id }}}" data-id="{{{ $image->id }}}" title="Delete image" class="btn btn-default btn-xs">
                                                            <i class="fa fa-fw fa-times"></i>
                                                        </button>
                                                        <button type="button" id="btnImageActivate-{{{ $image->id }}}" data-id="{{{ $image->id }}}" data-active="{{{ $image->active }}}" title="Image activation" class="btn color-primary-plus btn-xs">
                                                            @if ($image->active == 1)
                                                                <i class="fa fa-fw fa-check icon-white"></i>
                                                            @else
                                                                <i class="fa fa-fw fa-times-circle icon-white"></i>
                                                            @endif

                                                        </button>
                                                    </div>
                                                    <input id="gimg_order_index-{{{ $image->id }}}" data-id="{{{ $image->id  }}}" type="text" class="input-xs text-center" title="Image order index.&#13;Press enter to save." value="{{{ $image->order_index }}}">
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('admin.layouts.modal_confirmation')

        <div class="cbloader"></div>

        @if (!empty($initGallery))
            @include('admin.galleries.crop_modal')
        @endif
    </section>
@stop

    {{-- page level scripts --}}
@section('footer_scripts')
            <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/gallery/basic/lib/jquery.mousewheel.pack.js?v=3.1.3') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/gallery/basic/source/jquery.fancybox.pack.js?v=2.1.5') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/gallery/basic/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/gallery/basic/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/gallery/basic/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/fancybox-gallery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/imgcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/vendors/imgcrop/jquery.color.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/gallery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/process_image.js') }}" type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/modal_delete.js') }}"></script>

    <script>
        $(function () {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop