@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
View Band Details
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('josh/assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('josh/assets/css/pages/user_profile.css') }}" rel="stylesheet" type="text/css"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>View Band</h1>
    <ol class="breadcrumb">
        <li>
            <a href="index"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Bands</li>
        <li class="active">View band</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav  nav-tabs ">
                <li class="active">
                    <a href="#tab1" data-toggle="tab"> <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                        Profile
                    </a>
                </li>

            </ul>
            <div  class="tab-content mar-top">
                <div id="tab1" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        @lang('bands/title.band_profile')
                                    </h3>

                                </div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-file" data-trigger="fileinput">
                                                <img src="{{ asset($band->noimage) }}"  alt="..."></div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-max">
                                                @if ($band->image)
                                                    <img src="{{ $band->image }}" alt="...">
                                                @endif
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="..."></span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <form method="post" action="#">
                                                    <input type="hidden" id="domain" data-domain="bands" data-domain_id="{{ $band->id }}">

                                                <table class="table table-bordered table-striped" id="users">
    
                                                    <tr>
                                                        <td>@lang('bands/title.name')</td>
                                                        <td>
                                                            {{ $band->name }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>@lang('bands/title.description')</td>
                                                        <td>
                                                            {{ $band->description }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>@lang('bands/title.created_at')</td>
                                                        <td>
                                                            {{{ $band->created_at->diffForHumans() }}}
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- Bootstrap WYSIHTML5 -->
<script  src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
<script  src="{{ asset('josh/assets/js/pages/image_upload.js') }}" type="text/javascript"></script>
@stop