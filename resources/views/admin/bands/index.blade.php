@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Bands List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('josh/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('josh/assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Band</h1>
    <ol class="breadcrumb">
        <li>
            <a href="index"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Bands</li>
        <li class="active">Bands</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Band info
                </h4>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($bands as $band)
                    	<tr>
                            <td>{{{ $band->id }}}</td>
                    		<td>{{{ $band->name }}}</td>
            				<td>{{{ $band->description }}}</td>
            				<td>{{{ $band->created_at->diffForHumans() }}}</td>
            				<td>
                                <a href="{{ route('band.show', $band->id) }}">
                                    <i class="livicon" data-name="info" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view band"></i></a>

                                <a href="{{ route('band.edit', $band->id) }}">
                                    <i class="livicon" data-name="edit" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update band"></i></a>

                            </td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('josh/assets/vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('josh/assets/vendors/datatables/dataTables.bootstrap.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#table').DataTable();
    });
</script>
@stop