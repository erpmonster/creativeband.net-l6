@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
View User Details
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('josh/assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('josh/assets/css/pages/user_profile.css') }}" rel="stylesheet" type="text/css"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>View User</h1>
    <ol class="breadcrumb">
        <li>
            <a href="index"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Users</li>
        <li class="active">View User</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav  nav-tabs ">
                <li class="active">
                    <a href="#tab1" data-toggle="tab"> <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                        User Profile
                    </a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab">
                        <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                        Change Password
                    </a>
                </li>

            </ul>
            <div  class="tab-content mar-top">
                <div id="tab1" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        @lang('users/title.user_profile')
                                    </h3>

                                </div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-file" data-trigger="fileinput">
                                                <img src="{{ asset($user->noimage) }}"  alt="..."></div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-max">
                                                @if ($user->image)
                                                    <img src="{{ $user->image }}" alt="...">
                                                @endif
                                            </div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="..."></span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <form method="post" action="#">
                                                    <input type="hidden" id="domain" data-domain="users" data-domain_id="{{ $user->id }}">

                                                <table class="table table-bordered table-striped" id="users">
    
                                                    <tr>
                                                        <td>@lang('users/title.first_name')</td>
                                                        <td>
                                                            {{ $user->first_name }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>@lang('users/title.last_name')</td>
                                                        <td>
                                                            {{ $user->last_name }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>@lang('users/title.email')</td>
                                                        <td>
                                                            {{ $user->email }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>@lang('users/title.status')</td>
                                                        <td>
                                                        
                                                            @if($user->deleted_at)
                                                                Deleted
                                                            @elseif($user->activated)
                                                                Activated
                                                            @else
                                                                Pending
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>@lang('users/title.created_at')</td>
                                                        <td>
                                                            {{{ $user->created_at->diffForHumans() }}}
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('josh/assets/js/pages/image_upload.js') }}" type="text/javascript"></script>
@stop