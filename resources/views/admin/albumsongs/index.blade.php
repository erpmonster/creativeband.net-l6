@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Album songs manager
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')
            <!--page level css -->
    <link href="{{ asset('josh/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('josh/assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Album songs</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Albums</li>
            <li class="active">Album songs</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="clock" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Album songs management
                        </h3>
                    </div>
                    <div class="panel-body">

                        <!-- errors -->
                        <div class="has-error">
                            {{ $errors->first('order_index', '<span class="help-block">:message</span>') }}
                            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
                        </div>

                        <div class="portlet-body">

                            {{-- Albums panel --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="selAlbum" class="control-label">
                                            Select album
                                        </label>
                                        <div>
                                            <div class="col-md-6">
                                                <select id="selAlbum" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach ($albums as $album)
                                                        <option value="{{{ $album->id }}}"
                                                        @if ($album->id == $selectedAlbum->id) selected="selected" @endif >{{{ $album->title }}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group" id="btnAddSong">
                                                    <a class="btn btn-primary btn-large" data-toggle="modal" data-href="#albumsongsForm" href="#albumsongsForm">Add song
                                                        <i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr>

                            {{-- Songs panel --}}

                            @include('admin.albumsongs.song_modal')

                            <div>

                                @if (!empty($songs))

                                <table class="table table-striped table-bordered table-hover dataTable" id="table" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 90px">Order index</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Title
										: activate to sort column ascending">Title</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Duration
										: activate to sort column ascending" style="width: 80px">Duration</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Subtitle
										: activate to sort column ascending">Subtitle</th>
                                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="
											 Actions
										: activate to sort column ascending" style="width: 90px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($songs as $song)
                                        <tr>
                                            <td>{{{ $song->order_index }}}</td>
                                            <td>{{{ $song->title }}}</td>
                                            <td>{{{ $song->duration }}}</td>
                                            <td>{{{ $song->subtitle }}}</td>
                                            <td>
                                                <a data-toggle="modal" data-href="#albumsongsForm" href="#albumsongsForm" data-id="{{{ $song->id }}}" role="dialog">
                                                    <i class="livicon" data-name="edit" data-size="20" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update song"></i>
                                                </a>

                                                <a id="btnDelete" href="#modal_delete_confirm" data-toggle="modal" data-href="#modal_delete_confirm" data-route="{{{ route($model.'.delete', $song->id) }}}">
                                                    <i class="livicon" data-name="user-remove" data-size="20" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete song"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @if (isset($model))
            @include('admin.layouts.modal_confirmation')
        @endif

        <div class="cbloader"></div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('josh/assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/select2.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/modal/js/classie.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/modal/js/modalEffects.js') }}"></script>
    <script src="{{ asset('josh/assets/vendors/bootstrap-filestyle/src/bootstrap-filestyle.min.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('josh/assets/js/pages/albumsongs.js') }}"></script>
    <script src="{{ asset('josh/assets/js/pages/modal_delete.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });

            $('#table').DataTable();
        });
    </script>
@stop