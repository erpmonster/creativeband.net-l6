        <div class="modal fade in" id="albumsongsForm" tabindex="-1" role="dialog" aria-hidden="false" style="display:none;">
            <div class="modal-dialog modal-lg">
                <form class="form-horizontal" id="frmAddSong" action="#" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add song</h4>
                        </div>
                        <div class="modal-body">
                            <fieldset>

                                <input type="hidden" name="album_id" id="album_id" value="{{ $selectedAlbum->id }}">
                                <input type="hidden" name="song_id" id="song_id" value="">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="sname">Order index</label>
                                    <div class="col-md-9">
                                        <input id="order_index" name="order_index" type="text" placeholder="Order index" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">Title</label>
                                    <div class="col-md-9">
                                        <input id="title" name="title" type="text" placeholder="Song title" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Duration</label>
                                    <div class="col-md-9">
                                        <input id="duration" name="duration" type="text" placeholder="Song duration" class="form-control" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Subtitle</label>
                                    <div class="col-md-9">
                                        <input id="subtitle" name="subtitle" type="text" placeholder="Subtitle" class="form-control" value="">
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn">Close</button>
                            <button id="submit" type="submit" class="btn btn-primary">Ok</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>