
<!-- BEGIN CONCERTS SECTION -->
<section id="concerts">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title center">Termini</h1>
            </div>
            <div class="col-sm-12">
                <div class='col-sm-4'>
                    <form role="form" class="form-inline">
                        <div class="col-sm-6">
                            <input type='text' data-provide="datepicker" class="check-date" data-date-format="dd.mm.yyyy" data-date-autoclose="true">
                        </div>
                        <div class="col-sm-6">
                        <button type="submit" class="btn btn-default check_availability">Proveri</button>
                        </div>

                    </form>
                </div>
                <div id='calendar'></div>
            </div>
        </div>

    </div>

</section>
<!-- END CONCERTS SECTION -->


