
<!-- BEGIN GALLERY SECTION -->
<section id="gallery">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title center">Galerija</h1>
            </div>
        </div>
    </div>
    <!--<div class="gallery-scroller"> -->
    <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/007.jpg" itemprop="contentUrl" data-size="1200x800">
                <img src="gallery/thumb-007.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/008.jpg" itemprop="contentUrl" data-size="1200x800">
                <img src="gallery/thumb-008.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/009.jpg" itemprop="contentUrl" data-size="1200x900">
                <img src="gallery/thumb-009.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/001.jpg" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-001.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/002.jpg" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-002.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>

        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/003.jpg" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-003.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/004.png" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-004.png" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/005.jpg" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-005.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a href="gallery/006.jpg" itemprop="contentUrl" data-size="1200x1200">
                <img src="gallery/thumb-006.jpg" itemprop="thumbnail" alt="Image description" />
            </a>
        </figure>
    </div>
     <!--   </div> -->

    <!--
    <div class="gallery-scroller">
        <ul>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
        </ul>
        <ul>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
            <li>
                <a href="http://placehold.it/700x490" data-gal="prettyPhoto[gallery]" title="Lorem ipsum"><span></span><i class="hover-icon"></i></a>
                <img src="http://placehold.it/322x322" alt="" />
            </li>
        </ul>
    </div>
    -->
    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element, as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader-active when preloader is running
                      -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
</section>
<!-- END GALLERY SECTION -->
