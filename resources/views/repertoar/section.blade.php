<!-- BEGIN REPERTOAR SECTION -->
<section id="repertoar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title center">Kreativni Repertoar</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
    <p>    Najčešeće postavljeno pitanje svih onih koji žele da im Creative band svira na zabavi glasi:<br/>
    "Mozete li mi poslati vaš repertoar?" ili "Nismo našli vaš repertoar na sajtu..."<br/>
    </p>
    <p>
    Naš odgovor na pitanja te vrste glasi:
    Mi smo bend koji u skoro istoj postavi nastupa već deceniju i po i imamo godišnje preko 100 raznih nastupa,
    privatnih zabava, svadbi, žurki, korporativnih proslava, klupskih nastupa, koncerata... Logično je da na repertoaru
    imamo par hiljada pesama svih muzičkih žanrova, domaćeg i stranog popa i rocka, disco, house-a, evergreena,
    starogradskih pesama kao i narodnjaka onih starijih i ovih novijeg datuma, ukratko većina pesama koja se dokazala
    i opstala na svetskom i domaćem muzičkom nebu nalazi se na našem repertoaru. Sumnjam da bi bilo moguće postaviti
    naš repertoar u pisanoj formi a još manje pročitati ga u razumnom vremenskom periodu pa s toga repertoar u tom
    obliku nećete naći na ovoj stranici. :)
    </p>
    <p>
    Otvoreni smo za dogovor u vezi repertoara sa Vama u smislu, zamisili smo zabavu tako da preovladava strana muzika
    ili pak želimo više narodnjaka tog i tog tipa a manje ex Yu popa i sl. Nemojte nam tražiti spisak pesama koje
    ćemo izvoditi na vasoj zabavi zato sto to unapred ne mozemo znati, svaki iskusan muzicar vremenom stvori sesto
    čulo kojim osluškuje publiku i trudi se da pruži najbolje od mogućeg u datom trenutku a u skladu sa Vašim željama.
    Zbog svega gore navedenog mi i jesmo Creative bend i imamo uspeha na nastupima!
    </p>
            <p>
    Nadamo se da ste shvatili poentu a ukoliko želite čuti neke odredjene pesme na vašoj zabavi možete nam postaviti
    konkretno pitanje u vezi željenih pesama ili izvodjača.
            </p>
    </div>
        <div class="col-sm-offset-10 col-sm-2">
        </div>
    </div>
</section>
<!-- END REPERTOAR SECTION -->