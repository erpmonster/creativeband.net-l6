<!-- BEGIN CONTACTS SECTION -->
<section id="contacts" class="bg-color2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section-title center">Kontaktirajte nas</h1>
                <h2 class="center">Bratislav Milenković</h2>
                <h2 class="center">+381 (0) 63 451 708</h2>
                <div class="col-sm-5 center form-wrap">
                    <form method="post" action="#" class="bg-color1 form-contact">
                        <input type="text" placeholder="Vaše ime" name="contact_name" id="contact_name" />
                        <input type="text" placeholder="Lokacija" name="contact_location" id="contact_location" />
                        <input type="email" placeholder="Vaš E-mail" name="contact_email" id="contact_email" />
                        <input type="text" placeholder="Tema" name="contact_subject" id="contact_subject" />
                        <textarea cols="6" rows="4" placeholder="Poruka" name="contact_message" id="contact_message"></textarea>
                        <a href="" class="btn btn-dark btn-icon contact_send"><i class="icon-envelope"></i><span>Pošalji</span></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTACTS SECTION -->