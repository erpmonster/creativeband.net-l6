<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="utf-8">
</head>

<body>
	<h2>Kontakt forma</h2>
	<div>
		Pošiljaoc: {{ $name }}<br />
		E-mail: {{ $from }}<br />
		Tema: {{ $subject }}<br />
		Lokacija: {{ $location }}<br />
		Poruka: <br /> {{ $txtmessage }} <br />
	</div>
</body>

</html>