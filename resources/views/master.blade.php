<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />

    <!-- Page Title -->
    <title>Creative band - official site</title>

    <meta name="keywords" content="creative band, kreativ bend, kreativ, marija creative, ironija" />
    <meta name="description" content="Creative band official site" />
    <meta name="author" content="erpmonster" />

    <!-- Mobile Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!--  <link rel="shortcut icon" href="img/fav_touch_icons/favicon.jpg" />
    <link rel="apple-touch-icon" href="img/fav_touch_icons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/fav_touch_icons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/fav_touch_icons/apple-touch-icon-114x114.png" />
-->
    <!-- IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Google Web Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="js/photoswipe/photoswipe.css" rel="stylesheet" />
    <link href="js/photoswipe/default-skin/default-skin.css" rel="stylesheet" />
    <link href="css/fullcalendar.css" rel="stylesheet" />
    <link href="js/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <!-- Modernizr -->
    <script src="js/modernizr-2.6.2.min.js"></script>

</head>

<body>
    @include('home.partials.header')

    @include('home.partials.boxes')

    @include('news.overlay')

    @include('band.section')

    @include('concerts.section')

    @include('gallery.section')

    @include('repertoar.section')

    @include('contact.section')

    @include('home.partials.footer')

    <script src="music/myplaylist.js" type="text/javascript"></script>

    <!-- Libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')
    </script>
    <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js" type="text/javascript"></script>
    <script src="js/slideshow/slideshow.js" type="text/javascript"></script>
    <script src="js/photoswipe/photoswipe.js" type="text/javascript"></script>
    <script src="js/photoswipe/photoswipe-ui-default.min.js" type="text/javascript"></script>
    <script src="js/moment.min.js" type="text/javascript"></script>
    <script src="js/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="js/fullcalendar/gcal2.js" type="text/javascript"></script>
    <script src='js/fullcalendar/sr.js' type="text/javascript"></script>

    <script src='js/datepicker/js/bootstrap-datepicker.min.js' type="text/javascript"></script>

    <script src="js/scripts.js" type="text/javascript"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

    <script>
        var _gaq = [
            ['_setAccount', 'UA-XXXXX-X'],
            ['_trackPageview']
        ];
        (function(d, t) {
            var g = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            g.src = '//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g, s)
        }(document, 'script'));
    </script>

</body>

</html>