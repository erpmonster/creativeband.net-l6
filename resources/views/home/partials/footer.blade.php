<!-- BEGIN FOOTER -->
<footer id="footer" class="bg-color2">
    <ul class="sn-icons">
        <!-- BEGIN SOCIAL ICONS -->
        <li><a href="https://www.facebook.com/Creative-band-192318854225329/"><i class="icon-facebook-sign"></i></a></li>
        <li><a href="https://www.instagram.com/creative_band/"><i class="icon-instagram"></i></a></li>
    </ul><!-- END SOCIAL ICONS -->
    <p>Copyright &copy; 2015 Creative band. All rights reserved.</p>
</footer>
<!-- END FOOTER -->