<!-- BEGIN HEADER -->
<header id="header">
    <section class="nav-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="#home" class="nav-logo"><img src="img/logo_small.png" alt="" height="51" /></a>

                    <nav id="nav">
                        <!-- BEGIN MAIN MENU -->
                        <ul>
                            <li><a href="#theband">Bend</a></li>
                            <li><a href="#concerts">Termini</a></li>
                            <li><a href="#gallery">Galerija</a></li>
                            <li><a href="#repertoar">Repertoar</a></li>
                            <li><a href="#contacts">Kontakt</a></li>
                        </ul>
                    </nav><!-- END MAIN MENU -->

                    <ul class="sn-icons">
                        <!-- BEGIN SOCIAL ICONS -->
                        <li><a href="https://www.facebook.com/Creative-band-192318854225329/"><i class="icon-facebook-sign"></i></a></li>
                        <li><a href="https://www.instagram.com/creative_band/"><i class="icon-instagram"></i></a></li>
                    </ul><!-- END SOCIAL ICONS -->
                </div>
            </div>
        </div>
    </section>
</header>
<!-- END HEADER -->