<!-- BEGIN HOME SECTION -->
<section id="home">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="#home" id="logo" class="nav-logo"><img src="img/logo.png" alt="" width="379" height="205" /></a>

                <div id="social-stream">
                    <ul id="social-stream-items">
                        <li class="inst">
                            <div class="icon color"><i></i></div>
                            <div class="title">
                                <h3>Pratite nas na Instagramu</h3>
                            </div>
                            <div class="instagram"></div><!-- This div (with class "instagram") will be automatically popuplated with a single photo from your Instagram feed -->
                        </li>
                        <li class="featured">
                            <div class="icon"><i class="icon-expand"></i></div>
                            <div class="title">
                                <h3><a href="https://www.youtube.com/playlist?list=PLrCfEIlzJDgpE204pDzLKDrN9bk61ZOCk" target="_blank" data-gal="prettyPhoto">Creative Live</a></h3>
                            </div>
                            <!--<iframe width="350" height="350" src="https://www.youtube.com/embed/videoseries?list=PLrCfEIlzJDgpE204pDzLKDrN9bk61ZOCk" frameborder="0" allowfullscreen></iframe>-->

                            <a href="https://www.youtube.com/playlist?list=PLrCfEIlzJDgpE204pDzLKDrN9bk61ZOCk" target="_blank" data-gal="prettyPhoto">
                                <img src="img/video/video_play_list.png" alt="" /></a>
                        </li>
                        <li class="fb">
                            <div class="icon"><i class="icon-facebook"></i></div>
                            <div class="title">
                                <h3>Pratite nas na Facebooku</h3>
                            </div>
                            <div class="facebook"></div>
                        </li>

                        <li>
                            <!--<div class="icon color"><i class="icon-file-text-alt"></i></div> -->
                            <!--<div class="title"><h3 class="color"><a href="#" data-gal="prettyPhoto">Termini</a></h3></div>-->
                            <div class="termin-box">
                                <p><a href="#concerts">
                                        <h5>Kliknite ovde za pretragu slobodnih termina</h5>
                                    </a></p>
                                <div id="calendar2"></div>
                            </div>

                        </li>
                        <li>
                            <div class="icon color"><i class="icon-file-text-alt"></i></div>
                            <div class="title">
                                <h3 class="color">Kontakt</h3>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 center">

                                    <form id="form-contact0" method="post" action="#" class="bg-color1 form-contact">
                                        <h4 style="margin-bottom: 10px;margin-top: -20px;">Bata +381 (0) 63 451 708</h4>
                                        <div class="row">

                                            <div class="col-xs-6">
                                                <input type="text" placeholder="Ime" name="contact_name" id="contact_name" />
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" placeholder="Lokacija" name="contact_location" id="contact_location" />
                                            </div>

                                        </div>

                                        <input type="email" placeholder="Email" name="contact_email" id="contact_email" />

                                        <textarea cols="6" rows="2" placeholder="Poruka" name="contact_message" id="contact_message"></textarea>

                                        <a href="" class="btn btn-dark btn-icon contact_send"><i class="icon-envelope"></i><span>Pošalji</span></a>

                                    </form>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="icon color"><i class="icon-list"></i></div>
                            <div class="title">
                                <h3>Repertoar</h3>
                            </div>
                            <div class="repertoar-box" id="repertoar-box">
                                Najčešeće postavljeno pitanje svih onih koji žele da im Creative band svira na zabavi glasi:
                                "Mozete li mi poslati vaš repertoar?" ili "Nismo našli vaš repertoar na sajtu..."
                                Naš odgovor na pitanja te vrste glasi: Mi smo bend koji u skoro istoj postavi nastupa već deceniju i po i imamo godišnje preko 100 raznih nastupa, privatnih zabava, svadbi, žurki, korporativnih proslava, klupskih nastupa, koncerata... Logično je da na repertoaru imamo par hiljada pesama svih muzičkih žanrova, domaćeg i stranog popa i rocka, disco, house-a, evergreena, starogradskih pesama kao i narodnjaka onih starijih i ovih novijeg datuma, ukratko većina pesama koja se dokazala i opstala na svetskom i domaćem muzičkom nebu nalazi se na našem repertoaru. Sumnjam da bi bilo moguće postaviti naš repertoar u pisanoj formi a još manje pročitati ga u razumnom vremenskom periodu pa s toga repertoar u tom obliku nećete naći na ovoj stranici. :)

                                Otvoreni smo za dogovor u vezi repertoara sa Vama u smislu, zamisili smo zabavu tako da preovladava strana muzika ili pak želimo više narodnjaka tog i tog tipa a manje ex Yu popa i sl. Nemojte nam tražiti spisak pesama koje ćemo izvoditi na vasoj zabavi zato sto to unapred ne mozemo znati, svaki iskusan muzicar vremenom stvori sesto čulo kojim osluškuje publiku i trudi se da pruži najbolje od mogućeg u datom trenutku a u skladu sa Vašim željama. Zbog svega gore navedenog mi i jesmo Creative bend i imamo uspeha na nastupima!

                                Nadamo se da ste shvatili poentu a ukoliko želite čuti neke odredjene pesme na vašoj zabavi možete nam postaviti konkretno pitanje u vezi željenih pesama ili izvodjača.
                            </div>
                        </li>
                        <li>
                            <div class="icon color"><i class="icon-music"></i></div>
                            <div class="title">
                                <h3><a href="">Kreativni player</a></h3>
                            </div>
                            <div id="music-player"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="bottom-home"></div>
</section>
<!-- END HOME SECTION -->