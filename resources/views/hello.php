<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Creative band</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 400px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -175px;
			margin-top: -250px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}

		#clockdiv{
			font-family: sans-serif;
			color: #fff;
			display: inline-block;
			font-weight: 100;
			text-align: center;
			font-size: 30px;
			margin-top: 15px;
			margin-bottom: 15px;
		}

		#clockdiv > div{
			padding: 10px;
			border-radius: 3px;
			background: #9b989c;
			display: inline-block;
		}

		#clockdiv div > span{
			padding: 15px;
			border-radius: 3px;
			background: #555456;
			display: inline-block;
		}

		.smalltext{
			padding-top: 5px;
			font-size: 16px;
		}
		a:link {
			color: #555456;
		}

	</style>
</head>
<body>
	<div class="welcome">
		<h1>Novi sajt je u pripremi!</h1>
		<div id="clockdiv">
			<div>
				<span class="days"></span>
				<div class="smalltext">Dana</div>
			</div>
			<div>
				<span class="hours"></span>
				<div class="smalltext">Sati</div>
			</div>
			<div>
				<span class="minutes"></span>
				<div class="smalltext">Minuta</div>
			</div>
			<div>
				<span class="seconds"></span>
				<div class="smalltext">Sekundi</div>
			</div>
		</div>
		<a href="http://creativeband.net" title="Creative band"><img src="img/logo-black-hello.png" alt="Creative band"></a>

		<h2>Kontakt</h2>
		<h3>Tel: 063 451 708</h3>
		<h3>E-Mail: <a href="mailto:batacreative@gmail.com">batacreative@gmail.com</a></h3>
	</div>
<script>
	function getTimeRemaining(endtime){
		var t = Date.parse(endtime) - Date.parse(new Date());
		var seconds = Math.floor( (t/1000) % 60 );
		var minutes = Math.floor( (t/1000/60) % 60 );
		var hours = Math.floor( (t/(1000*60*60)) % 24 );
		var days = Math.floor( t/(1000*60*60*24) );
		return {
			'total': t,
			'days': days,
			'hours': hours,
			'minutes': minutes,
			'seconds': seconds
		};
	}

	function initializeClock(id, endtime){
		var clock = document.getElementById(id);
		var daysSpan = clock.querySelector('.days');
		var hoursSpan = clock.querySelector('.hours');
		var minutesSpan = clock.querySelector('.minutes');
		var secondsSpan = clock.querySelector('.seconds');

		function updateClock(){
			var t = getTimeRemaining(endtime);

			daysSpan.innerHTML = t.days;
			hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
			minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
			secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

			if(t.total<=0){
				clearInterval(timeinterval);
			}
		}

		updateClock();
		var timeinterval = setInterval(updateClock,1000);
	}

	var deadline = 'November 27 2015 02:00:00 UTC+0200';
	initializeClock('clockdiv', deadline);
</script>
</body>
</html>
