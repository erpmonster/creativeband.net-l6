<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home']);
Route::post('/api/instagram/feed', ['as' => 'apiInstagramGetFeed', 'uses' => 'HomeController@apiInstagramGetFeed']);
Route::get('/facebook/feed', ['as' => 'apiFacebookGetFeed', 'uses' => 'HomeController@apiFacebookGetFeed']);
Route::post('/api/facebook/feed', ['as' => 'apiFacebookGetFeed', 'uses' => 'HomeController@apiFacebookGetFeed']);
Route::get('/api/facebook/test', ['as' => 'apiFacebookTest', 'uses' => 'HomeController@testFb']);
Route::get('/api/google/calendar', ['as' => 'calendar', 'uses' => 'HomeController@calendar']);
Route::post('/api/mail/contact', ['as' => 'contact', 'uses' => 'HomeController@contactMessage']);


Route::group(array('prefix' => 'admin'), function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return View::make('admin/404');
    });
    Route::get('500', function () {
        return View::make('admin/500');
    });

    # All basic routes defined here
    Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
    Route::post('signin', 'AuthController@postSignin');
    Route::post('signup', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
    Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
    Route::get('login2', function () {
        return View::make('admin/login2');
    });

    # Register2
    Route::get('register2', function () {
        return View::make('admin/register2');
    });
    Route::post('register2', array('as' => 'register2', 'uses' => 'AuthController@postRegister2'));

    # Forgot Password Confirmation
    Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
    Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

    # Logout
    Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

    # Account Activation
    Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

    # Dashboard / Index
    Route::get('/', array('as' => 'dashboard', 'uses' => 'JoshController@showHome'));

    # User Management
    Route::group(array('prefix' => 'users', 'before' => 'Sentry'), function () {
        Route::get('/',                         array('as' => 'users',          'uses' => 'UsersController@getIndex'));
        Route::get('create',                    array('as' => 'create/user',    'uses' => 'UsersController@getCreate'));
        Route::post('create', 'UsersController@postCreate');
        Route::get('{userId}/edit',             array('as' => 'users.update',   'uses' => 'UsersController@getEdit'));
        Route::post('{userId}/edit', 'UsersController@postEdit');
        Route::get('{userId}/delete',           array('as' => 'delete/user',    'uses' => 'UsersController@getDelete'));
        Route::get('{userId}/confirm-delete',   array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore',          array('as' => 'restore/user',   'uses' => 'UsersController@getRestore'));
        Route::get('{userId}',                  array('as' => 'users.show',     'uses' => 'UsersController@show'));
        Route::get('api/getlist/users',         array('as' => 'api.users',      'uses' => 'UsersController@getUsersList'));
    });
    Route::get('deleted_users', array('as' => 'deleted_users', 'uses' => 'UsersController@getDeletedUsers'));

    # Band Management
    Route::group(array('prefix' => 'bands', 'before' => 'Sentry'), function () {
        Route::get('/',                     array('as' => 'band.index',     'uses' => 'BandController@index'));
        Route::post('create',               array('as' => 'band.post',      'uses' => 'BandController@store'));
        Route::get('show/{id?}',            array('as' => 'band.show',      'uses' => 'BandController@show'));
        Route::get('{id}/edit',             array('as' => 'band.edit',      'uses' => 'BandController@edit'));
        Route::put('{id}',                  array('as' => 'band.update',    'uses' => 'BandController@update'));
        Route::delete('{id}',               array('as' => 'band.destroy',   'uses' => 'BandController@destroy'));
        Route::get('confirm-delete/{id}/',  array('as' => 'confirm-delete.band', 'uses' => 'BandController@getModalDelete'));
    });

    # Band Members Management
    Route::group(array('prefix' => 'members', 'before' => 'Sentry'), function () {
        Route::get('/',                   array('as' => 'members.index',      'uses' => 'BandMemberController@index'));
        Route::get('create',              array('as' => 'members.create',     'uses' => 'BandMemberController@create'));
        Route::post('create',             array('as' => 'members.store',      'uses' => 'BandMemberController@store'));
        Route::get('{id}/edit',           array('as' => 'members.edit',       'uses' => 'BandMemberController@edit'));
        Route::put('{id}',                array('as' => 'members.update',     'uses' => 'BandMemberController@update'));

        Route::post('/', 'BandController@postMember');
        Route::get('api/{id}',            array('as' => 'api.members.record', 'uses' => 'BandMemberController@apiRecord'));
        Route::get('delete/{id}',         array('as' => 'members.delete',     'uses' => 'BandMemberController@destroy'));
    });

    Route::post('addb64image/{domain}/{domain_id}', 'ImageUploadController@addB64Image');
    Route::post('addimage/{domain}/{domain_id}',    'ImageUploadController@addImage');
    Route::get('deleteimage/{domain}/{domain_id}',  'ImageUploadController@deleteImage');

    # Galleries Management
    Route::group(array('prefix' => 'galleries', 'before' => 'Sentry'), function () {
        Route::get('/{id?}',                            array('as' => 'galleries',          'uses' => 'GalleriesController@index'));
        Route::get('/create/{name}',                    array('as' => 'galleries.create',   'uses' => 'GalleriesController@createGallery'));
        Route::get('/edit/{field}/{id}/{value}',        array('as' => 'galleries.edit',     'uses' => 'GalleriesController@editGallery'));
        Route::get('/delete/{id}',                      array('as' => 'galleries.delete',   'uses' => 'GalleriesController@deleteGallery'));
        Route::get('/edit/image/{field}/{id}/{value}',  array('as' => 'image.edit',         'uses' => 'GalleryImagesController@editImage'));
    });

    # Albums Management
    Route::group(array('prefix' => 'albums', 'before' => 'Sentry'), function () {
        Route::get('/',             array('as' => 'albums',         'uses' => 'AlbumController@index'));
        Route::post('/', 'AlbumController@postAlbum');
        Route::get('api/{id}',      array('as' => 'api.album.get',  'uses' => 'AlbumController@getAlbum'));
        Route::get('/delete/{id}',  array('as' => 'albums.delete',  'uses' => 'AlbumController@deleteAlbum'));
    });

    # Album song Management
    Route::group(array('prefix' => 'albumsongs', 'before' => 'Sentry'), function () {
        Route::get('/',             array('as' => 'albumsongs',   'uses' => 'AlbumSongController@index'));
        Route::get('/{id}',         array('as' => 'songs.show',   'uses' => 'AlbumSongController@showSongs'));
        Route::post('/{id}', 'AlbumSongController@postSong');
        Route::get('api/{id}',      array('as' => 'songs.get',    'uses' => 'AlbumSongController@getSong'));
        Route::get('/delete/{id}',  array('as' => 'songs.delete', 'uses' => 'AlbumSongController@deleteSongs'));
    });

    # Playlist Management
    Route::group(array('prefix' => 'playlists', 'before' => 'Sentry'), function () {
        Route::get('/',             array('as' => 'playlists',        'uses' => 'PlaylistController@index'));
        Route::post('/', 'PlaylistController@postItem');
        Route::get('api/{id}',      array('as' => 'playlists.get',    'uses' => 'PlaylistController@getItem'));
        Route::get('/delete/{id}',  array('as' => 'playlists.delete', 'uses' => 'PlaylistController@deleteItem'));
    });

    # Remaining pages will be called from below controller method
    # in real world scenario, you may be required to define all routes manually
    Route::get('{name?}', 'JoshController@showView');
});
