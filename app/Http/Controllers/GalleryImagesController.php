<?php


class GalleryImagesController extends BaseController
{

    public function editImage($field, $image_id, $value) {

        try {
            $image = GalleryImages::find($image_id);
            $image->$field = $value;

            $image->save();

            return 'true';
        } catch (Exception $e) {
            return 'false';
        }

    }

}