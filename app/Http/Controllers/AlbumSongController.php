<?php

class AlbumSongController extends BaseController
{
    public function index() {

        $albums = Album::all();

        $selectedAlbum = new StdClass();
        $selectedAlbum->id = null;

        return View::make('admin.albumsongs.index', compact('albums', 'selectedAlbum'));

    }

    public function showSongs($album_id) {

        $albums = Album::all();

        $selectedAlbum = DB::table('albums')->select('id')->where('id', '=', $album_id)->first();

        $songs = AlbumSong::where('album_id','=',$album_id)->get();

        $model = 'songs';

        return View::make('admin.albumsongs.index', compact('albums', 'songs', 'selectedAlbum', 'model'));

    }

    public function postSong($album_id) {

        $rules = array(
            'order_index' => 'required|numeric',
            'title'  => 'required|min:2'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator->messages());
        }

        try {
            $song = AlbumSong::firstOrNew(array('id'=>Input::get('song_id')));
            $song->album_id = $album_id;
            $song->order_index = Input::get('order_index');
            $song->title = Input::get('title');
            $song->duration = Input::get('duration');
            $song->subtitle = Input::get('subtitle');

            $song->save();

            return Redirect::route('songs.show', $album_id);
        } catch (Exception $e) {
            $error = '';

            return Redirect::to('admin/albums')->with('error', $error);
        }

    }

    public function getSong($song_id) {

        $song = AlbumSong::find($song_id);

        return Response::json($song);

    }

    public function deleteSongs($song_id) {

        $song = AlbumSong::find($song_id);
        $album_id= $song->album_id;

        $song->delete();

        return Redirect::route('songs.show', $album_id);

    }

}