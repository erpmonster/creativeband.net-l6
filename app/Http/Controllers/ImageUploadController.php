<?php


class ImageUploadController extends BaseController
{
    public function addB64Image($domain, $domain_id) {

        $request = Request::instance();
        $img_data = $request->getContent();

        $imagePath = ImageFileManager::Save_base64_image($img_data, $domain);

        if (!empty($imagePath)) {

            if (!ImageDBManager::SaveImage($imagePath, $domain, $domain_id)) {
                $error = '';

                return Redirect::back()->with('error', $error);
            }
        }
    }

    public function addImage($domain, $domain_id) {

        $request = Request::instance();

        $img_data = $request->file('image');

        if ($img_data) {

            $result = ImageFileManager::Save_image($img_data, $domain);

            if (!empty($result)) {

                if (!ImageDBManager::SaveImage($result, $domain, $domain_id)) {
                    $error = '';

                    return Redirect::back()->with('error', $error);
                } else {
                    return Redirect::route('galleries', $domain_id);
                }
            }
        }
    }

    public function deleteImage($domain, $domain_id) {

        $imagesToDelete = ImageDBManager::DeleteImage($domain, $domain_id);

        if (!empty($imagesToDelete)) {
            $result = ImageFileManager::DeleteImage($imagesToDelete);

            return ($result) ? 'true':'false';
        }

    }
}