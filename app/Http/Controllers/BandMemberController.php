<?php

class BandMemberController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
    {
		$members = BandMember::all();
		$model = 'members';

		return View::make('admin.members.index', compact('members', 'model'));

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.members.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
			'first_name'	=> 'required',
			'last_name'		=> 'required',
			'role'        	=> 'required',
            'description' 	=> 'required',
			'order_index' 	=> 'required|numeric'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}

        $request = Request::instance();
        $img_data = $request->file('image');

		try {
            $bandmember = new BandMember();
            $bandmember->first_name 	= Input::get('first_name');
            $bandmember->last_name 		= Input::get('last_name');
            $bandmember->role 			= Input::get('role');
            $bandmember->description 	= Input::get('description');
            $bandmember->order_index 	= Input::get('order_index');

            if (!empty($img_data)) {
                $path = '/img/members/';
                $fileName = $img_data->getClientOriginalName();
                $filePathName = '.'. $path;

                $bandmember->image = $path. $fileName;
            }

            $bandmember->save();

            if (!empty($img_data)) {
                $img_data->move($filePathName, $fileName);
            }

			return Redirect::route('members.index')->with('success', Lang::get('db/message.success.create'));
		} catch (Exception $e) {
			return Redirect::route('members.index')->with('success', Lang::get('db/message.error.create'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bandmember = BandMember::find($id);

		return View::make('admin.members.edit', compact('bandmember'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id = null) {

		try {

			$bandmember = BandMember::findOrFail($id);

		} catch (Exception $e) {

			$error = Lang::get('db/message.record_not_found', compact('id'));

			return Redirect::route('members.index')->with('error', $error);
		}

		$rules = array(
			'first_name'    => 'required',
			'last_name'     => 'required',
			'role'       	=> 'required|min:2',
			'order_index'   => 'required',
			'description' 	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}

        $request = Request::instance();
        $img_data = $request->file('image');


		try {
			$bandmember->first_name   = Input::get('first_name');
			$bandmember->last_name    = Input::get('last_name');
			$bandmember->role 		  = Input::get('role');
			$bandmember->order_index  = Input::get('order_index');
			$bandmember->description  = Input::get('description');

            if (!empty($img_data)) {
                $path = '/img/members/';
                $fileName = $img_data->getClientOriginalName();
                $filePathName = '.'. $path;

                $bandmember->image = $path. $fileName;
            }

			$bandmember->save();

            if (!empty($img_data)) {
                $img_data->move($filePathName, $fileName);
            }

			return Redirect::route("members.index")->with('success', Lang::get('db/message.success.update'));

		} catch (Exception $e) {
			$error = Lang::get('db/message.error.update');
		}

		return Redirect::back()->withInput()->with('error', $error);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
            BandMember::destroy($id);

            return Redirect::route('members.index')->with('success', Lang::get('db/message.success.delete'));
        } catch (Exception $e) {
            return Redirect::route('members.index')->with('success', Lang::get('db/message.error.delete'));
        }
	}

	public function apiRecord($id){

		$member = BandMember::find($id);

		return Response::json($member);
	}

}
