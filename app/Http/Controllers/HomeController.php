<?php


namespace App\Http\Controllers;

use App\Models\admin\Band;
use App\Models\GoogleCalendar;
use App\Models\Instagram;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Symfony\Component\Console\Input\Input;

class HomeController extends Controller
{

    /*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function showWelcome()
    {
        return View::make('hello');
    }

    public function home()
    {
        $band = Band::find(1);
        return View::make('master', compact('band'));
    }

    public function apiInstagramGetFeed()
    {
        $instagram = new Instagram();
        return json_encode($instagram->fetchData());
    }

    public function apiFacebookGetFeed()
    {

        //Replace with your Facebook Page ID
        $fb_page_id = '192318854225329';

        //Replace with your Access Token
        $access_token = 'EAAXPr2WK0AYBAB24afO5e6DqeMDrTK6ZAKdx7rE2Tp63rTiQBoaFhJUdnsjVOs4y5eqCZCNKxRX24PDdpmqDhFABOr0PMa2NzZCKpZAh9VY1SqEHXFgikQmmePEcqJ83Fg4dHuoyN1Jy3FGYZCZAVtl4j4fc0kXjwZD';

        # Don't need to edit below this line ##################################################################################
        #######################################################################################################################

        $graph_url = "https://graph.facebook.com/v5.0/" . $fb_page_id;
        $postData = "date_format=U&debug=all&fields=feed%7Bfrom%2Cmessage%2Cstory%2Cid%2Cpermalink_url%2Cfull_picture%2Ccreated_time%7D&format=json&method=get&pretty=0&suppress_http_code=1&transport=cors&access_token=" . $access_token;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $graph_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);

        curl_close($ch);

        echo $output;
    }

    public function calendar()
    {
        $calendar = new GoogleCalendar();
        $calendarId = 'marcic74@gmail.com';

        $optParams = array(
            'maxResults' => 600,
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => date('c'),
        );

        $events = $calendar->service->events->listEvents($calendarId, $optParams);


        if (count($events->getItems()) == 0) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events->getItems() as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }
    }

    public function contactMessage(Request $request)
    {
        $toMail = 'batacreative@gmail.com';
        $result = 'ok';


        $name = $request->get('name', '');
        $from = $request->get('email', '');
        $subject = $request->get('subject', '');
        $message = $request->get('message', '');
        $location = $request->get('location', '');

        try {
            // Data to be used on the email view
            $data = array(
                'name'              => $name,
                'location'          => $location,
                'from'              => $from,
                'subject'           => $subject,
                'txtmessage'        => $message,
            );

            // Send the activation code through email
            Mail::send('emails.contact', $data, function ($m) use ($toMail) {
                $m->to($toMail, 'Bata Creative');
                $m->subject('Kontakt forma');
            });
        } catch (Exception $e) {
            $result = $e->getMessage();
        }

        //  Redirect to the forgot password
        return $result;
    }
}
