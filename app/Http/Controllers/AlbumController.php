<?php


class AlbumController extends BaseController
{
    public function index(){

        $albums = Album::all();

        $model = 'albums';

        return View::make('admin.albums.index', compact('albums', 'model'));

    }

    public function postAlbum() {

        $rules = array(
            'title' => 'required|min:3',
            'year'  => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator->messages());
        }

        $request = Request::instance();
        $img_data = $request->file('image');

        try {

            $album = Album::firstOrNew(array('id'=>Input::get('album_id')));
            $album->title = Input::get('title');
            $album->year = Input::get('year');
            $album->description = Input::get('description');
            $album->description_2 = Input::get('description_2');

            if (!empty($img_data)) {
                $path = '/albums/';
                $fileName = $img_data->getClientOriginalName();
                $filePathName = '.'. $path;

                $album->image = $path. $fileName;
            }

            $album->save();

            if (!empty($img_data)) {
                $img_data->move($filePathName, $fileName);
            }

            return Redirect::to('admin/albums');

        } catch (Exception $e) {
            $error = '';

            return Redirect::to('admin/albums')->with('error', $error);

        }
    }

    public function getAlbum($id) {

        $album = Album::find($id);

        return Response::json($album);

    }

    public function deleteAlbum($id) {

        $album = Album::find($id);

        $album->delete();

        return Redirect::to('admin/albums');

    }

}