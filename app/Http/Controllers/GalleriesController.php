<?php


class GalleriesController extends BaseController
{

    public function index($id = null) {

        $galleries = Gallery::all();

        $model = 'galleries';

        if ($galleries->count() > 0) {
            if (empty($id)) {
                $initGallery = DB::table('galleries')->select('id', 'name')->where('active', '=', 1)->orderBy('order_index')->first();

                if ($initGallery) {
                    $id = $initGallery->id;
                }
            } else {
                $initGallery = Gallery::find($id);
            }

            $images = array();

            if (!empty($id)) {

                $images = GalleryImages::where('gallery_id','=',$id)->get();
            }
        } else {
            $galleries = array();
            $initGallery = array();
            $images = array();
        }

        return View::make('admin.galleries.index', compact('galleries', 'initGallery', 'images', 'model'));

    }

    public function createGallery($name) {

        if (empty($name)) {
            $name = 'Gallery'. rand(1, 1000);
        }

        $gallery = Gallery::where('name','=',$name)->first();
        $order_index = DB::table('galleries')->count() + 1;

        if ($gallery) {

            $name = $name. '_'. rand(1, 1000);

        }

        try {
            $gallery = new Gallery;
            $gallery->name = $name;
            $gallery->active = 1;
            $gallery->order_index = $order_index;

            $gallery->save();

            return $gallery->id;
        } catch (Exception $e) {
            return '0';
        }
    }

    public function editGallery($field, $gallery_id, $value) {

        try {
            $gallery = Gallery::find($gallery_id);
            $gallery->$field = $value;

            $gallery->save();

            return 'true';
        } catch (Exception $e) {
            return 'false';
        }

    }

    public function deleteGallery($id) {
        try {
            $gallery = Gallery::find($id);

            $gallery_images = $gallery->images()->get();

            $images_to_delete = array();

            foreach ($gallery_images as $gi) {
                if (!empty($gi->image)) {
                    $images_to_delete[] = $gi->image;
                }

                if (!empty($gi->image_thumb)) {
                    $images_to_delete[] = $gi->image_thumb;
                }
            }

            ImageFileManager::DeleteImage($images_to_delete);

            $gallery->delete();

            return Redirect::route('galleries');
        } catch (Exception $e) {


            return Redirect::back()->with('error', Lang::get('galleries/message.error.delete'));
        }

    }

}