<?php


class BandController extends BaseController
{
    public function index()
    {
        $bands = Band::All();

        return View::make('admin.bands.index', compact('bands'));
    }

    public function store()
    {

        $rules = array(
            'name'       => 'required|min:2',
            'description' => 'min:4'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            $band = new Band;
            $band->name = Input::get('name');
            $band->description = Input::get('description');

            $band->save();

            return Redirect::route("bands")->with('success', Lang::get('bands/message.success.create'));

        } catch (Exception $e) {
            $error = Lang::get('bands/message.error.create');
		}

        return Redirect::back()->withInput()->with('error', $error);
    }

    public function show($id=null) {

        if (empty($id)) {
            $band = Band::all()->first();
        } else {
            $band = Band::find($id);
        }

        $band->noimage = 'img/no-image.png';

        return View::make('admin.bands.show', compact('band'));

    }

    public function edit($id = null)  {

        $band = Band::find($id);

        return View::make('admin.bands.edit', compact('band'));
    }

    public function update($id)
    {
        try {

            $band = Band::findOrFail($id);
        } catch (Exception $e) {

            $error = Lang::get('bands/message.band_not_found', compact('id'));

            return Redirect::route('band.index')->with('error', $error);
        }

        $rules = array(
            'name'       => 'required|min:2',
            'description' => 'min:4'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        try {
            $band->name = Input::get('name');
            $band->description = Input::get('description');

            $band->save();

            return Redirect::route("band.index")->with('success', Lang::get('bands/message.success.update'));

        } catch (Exception $e) {
            $error = Lang::get('bands/message.error.update');
        }

        return Redirect::back()->withInput()->with('error', $error);
    }
}