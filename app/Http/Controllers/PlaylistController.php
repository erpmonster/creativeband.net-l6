<?php

class PlaylistController extends BaseController
{

    public function index() {

        $playlists = Playlist::all();

        $model = 'playlists';

        return View::make('admin.playlists.index', compact('playlists', 'model'));

    }

    private function saveFiles($files) {

        foreach ($files as $file) {

            if (isset($file)) {

                $ext = $file->getClientOriginalExtension();

                if ($ext == 'mp3' || $ext == 'oga') {
                    $path = '/music/';
                } else {
                    $path = '/cover';
                }

                $file->move('.' . $path, $file->getClientOriginalName());
            }
        }
    }

    private function deleteFiles($item) {

        $filesList = array();

        if (!empty($item->mp3)) {

            $filePathName = './music/'. $item->mp3;

            if (File::exists($filePathName)) {

                $filesList[] = $filePathName;

            }
        }

        if (!empty($item->oga)) {

            $filePathName = './music/'. $item->oga;

            if (File::exists($filePathName)) {

                $filesList[] = $filePathName;

            }
        }

        if (!empty($item->cover)) {

            $filePathName = './cover/'. $item->cover;

            if (File::exists($filePathName)) {

                $filesList[] = $filePathName;

            }
        }

        File::delete($filesList);
    }

    public function postItem($id = null) {

        $rules = array(
            'title' => 'required|min:2',
            'artist'  => 'required|min:2',
            'order_index'  => 'required|numeric',
            'active'  => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return Redirect::back()->withInput()->withErrors($validator);
        }

        $files = Input::file();

        $mp3_filename = null;
        $oga_filename = null;
        $cover_filename = null;

        foreach ($files as $file) {

            if (isset($file)) {

                $ext = $file->getClientOriginalExtension();

                if ($ext == 'mp3') {
                    $mp3_filename = $file->getClientOriginalName();
                }

                if ($ext == 'oga') {
                    $oga_filename = $file->getClientOriginalName();
                }

                if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {
                    $cover_filename = $file->getClientOriginalName();
                }
            }
        }

        try {
            $pl_item = Playlist::firstOrNew(array('id' => $id));
            $pl_item->title = Input::get('title');
            $pl_item->artist = Input::get('artist');
            $pl_item->mp3 = $mp3_filename;
            $pl_item->oga = $oga_filename;
            $pl_item->duration = Input::get('duration');
            $pl_item->cover = $cover_filename;
            $pl_item->order_index = Input::get('order_index');
            $pl_item->active = Input::get('active');

            $pl_item->save();

            $this->saveFiles($files);

            return Redirect::route('playlists');
        } catch (Exception $e) {
            $error = '';

            return Redirect::to('playlists')->with('error', $error);
        }
    }

    public function getItem($id) {

        $pl_item = Playlist::find($id);

        return Response::json($pl_item);

    }

    public function deleteItem($id) {

        $pl_item = Playlist::find($id);

        $this->deleteFiles($pl_item);

        $pl_item->delete();

        return Redirect::route('playlists');

    }
}