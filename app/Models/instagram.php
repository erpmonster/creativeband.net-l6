<?php

namespace App\Models;

class Instagram
{

	private $user_id;
	private $access_token;
	private $number_photos;
	private $api_url;

	public function __construct()
	{
		$this->user_id = '2243383684'; //Replace with your user_id
		$this->access_token = '2243383684.d9c97e4.171b681d7a704db8a325b60b4e2ce514'; //Replace with your access_token

		$this->number_photos = '18';
		$this->api_url = 'https://api.instagram.com/v1/users/' . $this->user_id . '/media/recent/?access_token=' . $this->access_token . '&count=' . $this->number_photos;
	}

	public function fetchData()
	{

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}
