<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class GalleryImages extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery_images';
    protected $fillable = ['gallery_id', 'image', 'image_thumb', 'order_index', 'active'];

    public function gallery(){
        return $this->belongsTo('Gallery', 'gallery_id');
    }


}