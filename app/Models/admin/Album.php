<?php

class Album extends Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'albums';
    protected $fillable = ['title', 'year', 'description', 'description2', 'image'];


    public function songs() {
        return $this->hasMany('AlbumSong');
    }

}