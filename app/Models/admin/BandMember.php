<?php


class BandMember extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'band_members';
	protected $fillable = ['first_name', 'last_name', 'role', 'description', 'image', 'order_index'];


}