<?php

class AlbumSong extends Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'album_songs';
    protected $fillable = ['album_id', 'order_index', 'title', 'duration', 'subtitle'];


    public function album() {
        $this->belogsTo('Album');
    }
}