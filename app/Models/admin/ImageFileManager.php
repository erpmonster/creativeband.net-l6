<?php

class ImageFileManager
{
    private static $filename;
    private static $image;

    private static function decode_image($image_data){

        $data = explode(',', $image_data);
        $img_type = substr(strstr(strstr($data[0], ';', true), ':'), 1);

        self::$image = base64_decode($data[1]);

        if ($img_type == 'image/png'){
            $ext = '.png';
        } elseif ($img_type == 'image/jpg' || $img_type == 'image/jpeg') {
            $ext = '.jpg';
        }  elseif ($img_type == 'image/gif') {
            $ext = '.gif';
        }

        self::$filename = date('YmdHis'). rand(). $ext;
    }

    public static function Save_base64_image($image, $domain) {

        $result = '';

        self::decode_image($image);

        if (!empty(self::$filename)) {

            if ($domain == 'users' || $domain == 'members') {
                $path = '/img/members/';
            } elseif ($domain == 'bands') {
                $path = '/img/bands/';
            } elseif ($domain == 'albums') {
                $path = '/img/albums/';
            }

            $filePathName = '.'. $path. self::$filename;

            $size = File::put($filePathName , self::$image);

            if ($size > 0){
                $result = $path. self::$filename;
            }
        }

        return $result;
    }

    private static function getLastFileNr() {

        $value = DB::table('params')->select('param_value')->where('param_name','=','last_image_file_id')->first();

        if (empty($value)) {
            return 1;
        } else {
            return ((int)$value->param_value) + 1;
        }
    }

    private static function setLastFile($value) {
        DB::table('params')->where('param_name','=','last_image_file_id')->update(['param_value'=>$value]);
    }

    private static function createThumbnail($src, $ext, $thumb_info) {

        $targ_w = $targ_h = 322;
        $jpeg_quality = 90;

        $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

        if (strcasecmp($ext, '.jpg') == 0) {
            $img_r = imagecreatefromjpeg($src);
        } elseif (strcasecmp($ext, '.png') == 0) {
            $img_r = imagecreatefrompng($src);
        } elseif (strcasecmp($ext, '.gif') == 0) {
            $img_r = imagecreatefromgif($src);
        }

        imagecopyresampled($dst_r, $img_r, 0, 0, intval($thumb_info['img_x']), intval($thumb_info['img_y']), $targ_w, $targ_h, intval($thumb_info['img_w']), intval($thumb_info['img_h']));

        $thumbFilePath = '.'. $thumb_info['thumbPath'];

        if (strcasecmp($ext, '.jpg') == 0) {
            imagejpeg($dst_r, $thumbFilePath, $jpeg_quality);
        } elseif (strcasecmp($ext, '.png') == 0) {
            imagepng($dst_r,$thumbFilePath );
        } elseif (strcasecmp($ext, '.gif') == 0) {
            imagegif($dst_r, $thumbFilePath);
        }

    }

    public static function Save_image($image, $domain) {

        $fileIndex = self::getLastFileNr();
        $fileExt = '.'. pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);

        if ($domain == 'users' || $domain == 'members') {
            $path = '/img/members/';
        } elseif ($domain == 'bands') {
            $path = '/img/bands/';
        } elseif ($domain == 'albums') {
            $path = '/img/albums/';
        } elseif ($domain == 'galleries') {
            $path = '/gallery/';
        }

        $fileName = sprintf('%03d', $fileIndex). $fileExt;
        $filePathName = '.'. $path;

        $img_w = Input::get('img_w');
        if (!empty($img_w)) {
            $img_x = Input::get('img_x');
            $img_y = Input::get('img_y');
            $img_h = Input::get('img_h');

            $thumbPath = $path. 'thumb-'. $fileName;

            $thumb_info = compact('img_x', 'img_y', 'img_w', 'img_h', 'thumbPath');
        }

        try {
            $image->move($filePathName, $fileName);

            if (isset($thumb_info)) {
                self::createThumbnail($filePathName. $fileName, $fileExt, $thumb_info);
            }

            self::setLastFile($fileIndex);

            $result = array('imagePath' => $path. $fileName, 'thumbPath' => $thumbPath);
        } catch (Exception $e) {
            $result = '';
        }

        return $result;
    }

    public static function DeleteImage($imagesPathName) {

        foreach ($imagesPathName as $image) {

            $filePathName = '.' . $image;

            if (File::exists($filePathName)) {

                File::delete($filePathName);

            }
        }

        return true;
    }

}