<?php


class ImageDBManager
{

    public static function SaveImage($files, $domain, $domain_id) {

        try {
            if ($domain == 'users') {
                $user = Sentry::findUserById($domain_id);
                $user->image = $files['imagePath'];

                $user->save();

            } elseif ($domain == 'bands') {
                $band = Band::find($domain_id);
                $band->image = $files['imagePath'];

                $band->save();
            } elseif ($domain == 'members') {
                $member = BandMember::find($domain_id);
                $member->image = $files['imagePath'];

                $member->save();
            }
            elseif ($domain == 'galleries') {
                $filename = pathinfo($files['imagePath'], PATHINFO_BASENAME);

                $images = new GalleryImages;
                $images->gallery_id = $domain_id;
                $images->image = $files['imagePath'];
                $images->image_thumb = $files['thumbPath'];
                $images->order_index = 1;
                $images->active = 1;

                $images->save();
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function DeleteImage($domain, $domain_id) {

        $result = '';

        try {
            if ($domain == 'users') {
                $user = Sentry::findUserById($domain_id);
                $result = $user->image;

                $user->image = null;

                $user->save();

            } elseif ($domain == 'bands') {
                $band = Band::find($domain_id);
                $result = $band->image;

                $band->image = null;

                $band->save();
            } elseif ($domain == 'members') {
                $member = BandMember::find($domain_id);
                $result = $member->image;

                $member->image = null;

                $member->save();
            } elseif ($domain == 'galleries') {
                $image = GalleryImages::find($domain_id);
                $result = array($image->image, $image->image_thumb);

                $image->forceDelete();
            }

            return $result;
        } catch (Exception $e) {
            return '';
        }
    }
}