<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class Band extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bands';
    protected $fillable = ['name', 'description', 'image'];
}
