<?php

class Playlist extends Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'playlists';
    protected $fillable = ['mp3', 'oga', 'title', 'artist', 'duration', 'cover', 'order_index', 'active'];


}