/*
* Author: Wisely Themes
* Author URI: http://themeforest.net/user/wiselythemes
* Theme Name: Beat
* Version: 1.0.5
*/

/* global Modernizr:true, google:true, myConcerts:true, myPlaylist:true, RichMarker:true */

var Beat = {

	initialized: false,
	mobMenuFlag: false,
	wookHandler: null,
	wookOptions: null,
	scrollPos: 0,
	sendingMail: false,
	myLatlng: null,

	init: function() {
		"use strict";
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		var $tis = this;
		
		if ($tis.initialized){
			return;
		}
		
		$tis.initialized = true;
		$tis.construct();
		$tis.events();
	},

	construct: function() {
		"use strict";
		
		var $tis = this;
		
		/**
		 * Navigation
		 */
		$tis.navigation();
		
		/**
		 * Dinamically create the menu for mobile devices
		 */
		$tis.createMobileMenu();
		
		/**
		 * Social stream items navigation
		 */
		$tis.socialStream();
		
		/**
		 * Create Mp3 Player
		 */
		$tis.createMp3Player();
		
		/**
		 * Activate placeholder in older browsers
		 */
		$('input, textarea').placeholder();
		
		/**
		 * Initialize Google calendars
		 */
		$tis.getCalendar();
		
		/**
		 * Start NiceScroll
		 */
		$tis.startNiceScroll();
		
		/**
		 * Get Instagram feed
		 */
		$tis.getInstagram();
		
		/**
		 * Get Facebook feed
		 */
		$tis.getFacebook();
	},

	events: function() {
		"use strict";
		var $tis = this;
		
		/**
		 * Check if browser is a mobile device
		 */
		$tis.isMobile();
		
		/**
		 * Functions called on window resize
		 */
		$tis.windowResize();
		
		/**
		 * Resize logo on window resize
		 */
		$tis.resizeLogo();
		
		/**
		 * Overlay open/close buttons
		 */
		$tis.overlayButtons();
		
		/**
		 * Resize embed videos
		 */
		$tis.resizeVideos();
		
		/**
		 * Contact form submit
		 */
		$tis.contactForm();
		
		/**
		 * Capture buttons click event
		 */
		$tis.buttons();
		
	},
	
	navigation: function() {
		"use strict";
		
		$('#nav li a, .nav-logo').bind('click',function(event){
			var navActive = $(this);
			var scroll = 0;
			
			if (navActive.attr('href') !== "#home"){
				scroll = $(navActive.attr('href')).offset().top -65;
			}
			
			$('html, body').stop().animate({
				scrollTop: scroll
			}, 1500,'easeInOutExpo', function(){
				navActive.blur();
			});
			
			event.preventDefault();
		});
		
		$('.nav-section').waypoint('sticky', {
			handler: function(dir) {
				if(dir === "down"){
					$(this).css({opacity:0});
					$(this).animate({opacity:1}, 500);
				}
			},
			offset: -250
		});

		$('#logo').waypoint(function(dir) {
				if(dir === "down"){
					$(".nav-section .nav-logo").css({visibility: 'visible', opacity:1});
				} else {
					$(".nav-section .nav-logo").css({visibility: 'hidden', opacity:0});
				}
		}, { offset: -65 });
			
		$("section").waypoint(function(direction) {
			var tis = $(this);
			
			if (direction === "up"){ tis = tis.prev(); }
			
			$("#nav a").removeClass("active");
			$('nav a[href="#' + tis.attr("id") + '"]').addClass("active");
		}, { offset: '50%' });
	},
	
	createMobileMenu: function(w) {
		"use strict";
		var $tis = this;
		
		if ( w !== null ){
			w = $(window).innerWidth();
		}
		
		if (w <= 751 && !$tis.mobMenuFlag) {
			var select = document.createElement('select');
			var first = document.createElement('option');

			first.innerHTML = 'Menu';
			first.setAttribute('selected', 'selected');
			select.setAttribute('id', 'mobile-nav');
			select.appendChild(first);

			var nav = document.getElementById('nav');
			var loadLinks = function(element, hyphen, level) {

				var e = element;
				var children = e.children;

				for(var i = 0; i < e.children.length; ++i) {

					var currentLink = children[i];

					switch(currentLink.nodeName) {
						case 'A':
							var option = document.createElement('option');
							option.innerHTML = (level++ < 1 ? '' : hyphen) + currentLink.innerHTML;
							option.value = $(currentLink).attr('href');
							select.appendChild(option);
							break;
						default:
							if(currentLink.nodeName === 'UL') {
								if (level >= 2 ){
									hyphen += hyphen;
								}
							}
							loadLinks(currentLink, hyphen, level);
							break;
					}
				}
			};

			loadLinks(nav, '- ', 0);

			nav.appendChild(select);

			var mobileNavChange = function(navActive) {
				var scroll = 0;
					
				if (navActive !== "#home" && navActive !== "Menu"){
					scroll = $(navActive).offset().top -65;
				}
				
				$('html, body').stop().animate({
					scrollTop: scroll
				}, 1500,'easeInOutExpo');
			};
			
			var mobileNav = document.getElementById('mobile-nav');

			if(mobileNav.addEventListener) {
				mobileNav.addEventListener('change', function () {
					mobileNavChange(mobileNav.options[mobileNav.selectedIndex].value);
				});
			} else if(mobileNav.attachEvent) {
				mobileNav.attachEvent('onchange', function () {
					mobileNavChange(mobileNav.options[mobileNav.selectedIndex].value);
				});
			} else {
				mobileNav.onchange = function () {
					mobileNavChange(mobileNav.options[mobileNav.selectedIndex].value);
				};
			}
			
			$tis.mobMenuFlag = true;
		}
	},
	
	socialStream: function() {
		"use strict";
		var $tis = this;
		
		var initialItems = 6,
			items = $('#social-stream-items li'),
			numItems = items.length,
			numPages = Math.ceil(numItems/initialItems);
		
		//Create navigation bullets
		var nav = document.createElement('ol');
			
		nav.setAttribute('id', 'social-stream-nav');
		
		for(var i=0; i<numPages; i++){
			var elem = document.createElement('li');
			elem.setAttribute('data-filter', 'page'+(i+1) );
			nav.appendChild(elem);
		}
		
		$('#social-stream').after(nav);
		
		
		//Add data-filter-class to each social stream item
		var page = 1;
		items.each(function(i){
			if ( i+1 > page * initialItems){
				page++;
			}
			
			this.setAttribute('data-filter-class', '["page' + page + '"]');
		});
		
		
		//Items show animation
		var itemsAnim = function(activeFilter){
				$('#social-stream').addClass('hideShadow');
				
				items.filter(function() { 
					return $(this).data('filter-class').toString() === activeFilter.toString();
				}).each(function(i){
					var $this = $(this);
					if ( i<initialItems ){					
						setTimeout(function() {
							$this.addClass("enabled");
						}, 200*i);
					}
				});
				
				setTimeout(function() {
					$('#social-stream').removeClass('hideShadow');
				}, 200*(initialItems-1));
			};
		
	
		// Prepare layout options.
		$tis.wookOptions = {
			autoResize: true, // This will auto-update the layout when the browser window is resized.
			container: $('#social-stream'), // Optional, used for some extra CSS styling
			offset: 1, // Optional, the distance between grid items
			itemWidth: 350 // Optional, the width of a grid item
		};

		// Get a reference to your grid items.
		$tis.wookHandler = $('#social-stream-items li');
		var filters = $('#social-stream-nav li');

		// Call the layout function.
		$tis.wookHandler.wookmark($tis.wookOptions);

		/**
		 * When a filter is clicked, toggle it's active state and refresh.
		 */
		var itemFilter;
		var onClickFilter = function(event) {
			var item = $(event.currentTarget),
				activeFilters = [];
			
			itemFilter = item.data('filter');
			
			if (!item.hasClass('active')) {
				filters.removeClass('active');
			} else {
				return false;
			}
			item.toggleClass('active');

			// Filter by the currently selected filter
			if (item.hasClass('active')) {
				
				items.filter(function() { 
					return $(this).data('filter-class').toString() !== itemFilter.toString();
				}).removeClass("enabled");
				
				activeFilters.push(itemFilter);
			}

			$tis.wookHandler.wookmarkInstance.filter(activeFilters);
			itemsAnim(itemFilter);
			$.waypoints('refresh');
		};

		// Capture filter click events.
		filters.click(onClickFilter);
	
		$("#social-stream-nav li").eq(0).click();
	},
	
	createMp3Player: function() {
		"use strict";
		
		$(document).ready(function(){
			$('#music-player').ttwMusicPlayer(myPlaylist, {
				autoPlay:true,
				currencySymbol:'',
				buyText:'',
				tracksToShow:1000,
				/*ratingCallback:function(index, playlistItem, rating){
					//some logic to process the rating, perhaps through an ajax call
				},*/
				jPlayer:{
					swfPath:'js/jplayer'
				}
			});
		});
	},
	
	getCalendar: function() {
		"use strict";
		$('#calendar').fullCalendar({
			lang: 'sr',

			googleCalendarApiKey: 'AIzaSyAhnGWtkKdCHRl-dcGN5QcHL-yLP7m_BxE',
			events: 'batacreative@gmail.com',
			fixedWeekCount : true

		});

		$('#calendar2').fullCalendar({
			lang: 'sr',
			header : false,
			fixedWeekCount : false,
			contentHeight: 310,
			googleCalendarApiKey: 'AIzaSyAhnGWtkKdCHRl-dcGN5QcHL-yLP7m_BxE',
			events: 'batacreative@gmail.com',


		});

		$(".check-date").datepicker({
			format: "dd.mm.yyyy",
			autoclose: true,
			language: 'rs-latin'
		}).on('changeDate', function(ev) {
			$('#calendar').fullCalendar('gotoDate', ev.date);
		});
		$('.check_availability').on('click', function(e){
			e.preventDefault();
			$(".check-date").datepicker('show');
		});

	},

	startNiceScroll:function() {
		"use strict";
		
		$(document).ready(function(){
			$("html").niceScroll({
				styler:"fb",
				//autohidemode:true,
				cursorcolor:"#c2c2c2",
				cursoropacitymax:"0.7",
				cursorborder:"0px solid #000",
				horizrailenabled:false,
				zindex:"1001"
			});
			
			if (!$.browser.mobile) {
				$("#music-player .tracklist").niceScroll({
					cursorcolor:"#c2c2c2", 
					cursoropacitymax:"0.7",
					cursorborder:"0px solid #000",
					railpadding:{top:0,right:3,left:0,bottom:0}
				});
			}

			$("#repertoar-box").niceScroll({
				styler:"fb",
				autohidemode:true,
				cursorcolor:"#c2c2c2",
				cursoropacitymax:"0.7",
				cursorborder:"0px solid #000",
				zindex:"999"
			});

			$("#complete-list").niceScroll({
				cursorcolor:"#c2c2c2", 
				cursoropacitymax:"0.7",
				cursorborder:"0px solid #000",
				railpadding:{top:0,right:3,left:0,bottom:0},
				zindex:"999"
			});
			
			$(".my-gallery").niceScroll({
				cursorcolor:'#ee5656',
				cursorwidth:'20px',
				background:'#1F2326',
				cursorborder:'0px solid #1F2326',
				zindex:'999',
				autohidemode:false,
				enablemousewheel:false
			});
		});
		
		$("#music-player").one("mouseenter mouseleave", function(){
			$("#music-player .tracklist").getNiceScroll().resize();
		});
		
		$("#complete-list").on("mouseenter mouseleave", function(){
			$("#complete-list").getNiceScroll().resize();
		});
	},
	
	getInstagram:function() {
		"use strict";
		
		$('.instagram').html('<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>');
		
		$.ajax({
			type: 'post',
			url: 'api/instagram/feed',
			contentType: 'application/json',
			dataType: 'json',
			success: function(json){
				var feed = $.parseJSON(json),
					len = $(".instagram").length,
					index = 0,
					feedLen = 0;
				
				if( feed !== '' ){
					for(var key in feed.data){
						if (feed.data.hasOwnProperty(key)) {
							feedLen++;
						}
					}
				}
						
				for (var i = 0; i < feedLen; i++){
					if (index < len ){
						$(".instagram").eq(index).html('<a href="' + feed.data[i].link + '" target="_blank"><img src="' + feed.data[i].images.standard_resolution.url + '" alt="" /></a>');
						index++;
					} else {
						return false;
					}
				}
			},
			error: function(){
				//console.log("Error getting Instagram feed");
			}
		});
	},
	
	getFacebook:function() {
		"use strict";
		
		function timeDifference(current, previous) {

			var msPerMinute = 60 * 1000;
			var msPerHour = msPerMinute * 60;
			var msPerDay = msPerHour * 24;
			var msPerMonth = msPerDay * 30;
			var msPerYear = msPerDay * 365;

			var elapsed = current - previous;
			
			if (elapsed < msPerMinute) {
				 return Math.round(elapsed/1000) + ' seconds ago';   
			}

			else if (elapsed < msPerHour) {
				 return Math.round(elapsed/msPerMinute) + ' minutes ago';   
			}

			else if (elapsed < msPerDay ) {
				 return Math.round(elapsed/msPerHour ) + ' hours ago';   
			}

			else if (elapsed < msPerMonth) {
				return 'approximately ' + Math.round(elapsed/msPerDay) + ' days ago';   
			}

			else if (elapsed < msPerYear) {
				return 'approximately ' + Math.round(elapsed/msPerMonth) + ' months ago';   
			}

			else {
				return 'approximately ' + Math.round(elapsed/msPerYear ) + ' years ago';   
			}
		}
		
		function getLinkUrl(id, link) {
			if ( link.match(/www.facebook.com/g) ){				
				return link;
			} else {
				return "https://www.facebook.com/" + id;
			}
		}
		
		$('#social-stream .facebook').html('<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>');
		
		var xhr = $.ajax({
			type: 'get',
			url: '/facebook/feed',
			contentType: 'application/json',
			dataType: 'json',
			success: function(json){
				
				if (json == null){ 
					console.log("Error accessing Facebook feed");
				} else {
					var feed = json.feed,
						len = $(".facebook").length,
						index = 0,
						feedLen = json.feed.data.length,
						message = '',
						time = new Date().getTime();
					
					for (var i = 0; i < feedLen; i++){
						if (index < len ){
							if ( feed.data[i].message != undefined ){
								message = feed.data[i].message;
							} else  if ( feed.data[i].story != undefined ){
								message = feed.data[i].story;
							}
							
							if ( message.length > 140 ){
								message = message.slice(0,140) + "...";
							}
							
							$("#social-stream .facebook").eq(index).html('<a href="' + getLinkUrl(feed.data[i].id, feed.data[i].permalink_url) + '" target="_blank"><img src="' + feed.data[i].full_picture + '" alt="" /></a><div class="text"><h3>' + feed.data[i].from.name + ' &nbsp;<span>' + timeDifference(time, feed.data[i].created_time*1000) + '</span></h3>' + message + '</div>');
							index++;
						} else {
							return false;
						}
					}
				}
			},
			error: function(){
				console.log("Error getting Facebook feed");
			}
		});
	},
	
	windowResize:function() {
		"use strict";
		
		var $tis = this;
		
		$(window).resize(function() {
			var w = $(window).innerWidth();
			
			$tis.resizeLogo(w);
			$tis.createMobileMenu(w);
		});
	},
	
	resizeLogo:function(w) {
		"use strict";
		
		if ( w !== null ){
			w = $(window).innerWidth();
		}
		
		$("#logo").css({maxWidth: w + 'px'});
	},
	
	overlayButtons:function() {
		"use strict";
		var $tis = this;
		
		$(".open-overlay").click(function(e){
			e.preventDefault();
			
			var newsDetails = $(this).parent().data('news-details');
			if ( newsDetails !== undefined ){
				$tis.populateNews(newsDetails);
			}
			
			var page = $("#" + $(this).data('overlay-id'));
			
			$tis.scrollPos = $(window).scrollTop();
			
			var transEndEventNames = {
					'WebkitTransition' : 'webkitTransitionEnd',
					'OTransition' : 'oTransitionEnd',
					'msTransition' : 'MSTransitionEnd',
					'transition' : 'transitionend'
				},
				// animation end event name
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ];
				
			if ( transEndEventName === undefined){
				page.addClass('moveFromBottom');
				
				$("#header, section, #footer").hide();
				$("#music-player .tracklist").getNiceScroll().hide();
					
				$('html, body').animate({scrollTop: 0}, 0);
				page.css({position:'absolute'});
			
			} else {
				page.addClass('moveFromBottom').one( transEndEventName, function() {
					$("#header, section, #footer, .nicescroll-rails").hide();
					$(".nicescroll-rails:first").show();
					$("#music-player .tracklist").getNiceScroll().hide();
					
					$('html, body').animate({scrollTop: 0}, 0);
					$(this).css({position:'absolute'});
				});
			}
			
			page.on("mouseenter mouseleave", function(){
				$("html").getNiceScroll().resize();
			});
			
		});
		
		$(".close-overlay").click(function(){
			var page = $('.page-overlay');
			
			page.css({position:'fixed'});
			
			$("#header, section, #footer, .nicescroll-rails").show();
			$("#music-player .tracklist").getNiceScroll().show();
			
			$('html, body').animate({scrollTop: $tis.scrollPos}, 0);
			
			page.removeClass('moveFromBottom');
		});
	},
	
	resizeVideos: function(){
		"use strict";
		
		var $allVideos = $("iframe[src^='http://player.vimeo.com'], iframe[src^='http://www.youtube.com'], object, embed"),
			$fluidEl = $(".videoEmbed");
		
		$allVideos.each(function() {
			var $el = $(this);
			$el.attr('data-aspectRatio', $el.height() / $el.width()).removeAttr('height').removeAttr('width');
		});
		
		$(window).resize(function() {
			var newWidth = $fluidEl.width();
			
			$allVideos.each(function() {
				var $el = $(this);
				$el.width(newWidth).height(newWidth * $el.attr('data-aspectRatio'));
			});
		}).resize();
	},
	
	isMobile: function(){
		"use strict";
		
		(function(){(jQuery.browser=jQuery.browser||{}).mobile=(/android|webos|iphone|ipad|ipod|blackberry/i.test(navigator.userAgent.toLowerCase()));})(navigator.userAgent||navigator.vendor||window.opera);
	},
	
	contactForm: function() {
		"use strict";
		var $tis = this,
			form;
		$(".contact_send").click(function(e){
			e.preventDefault();
			form = this.closest('form');

			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
				name = $(form).find('#contact_name').val(),
				email = $(form).find('#contact_email').val(),
				subject = $(form).find('#contact_subject').val(),
				message = $(form).find('#contact_message').val(),
				location = $(form).find('#contact_location').val(),
				html = "",
				error = false;
			
			if(name === ""){
				$(form).find('#contact_name').addClass('invalid');
				error = true;
			}else{
				$(form).find('#contact_name').removeClass('invalid');
				html = "name=" + name;
			}

			if(location === ""){
				$(form).find('#contact_location').addClass('invalid');
				error = true;
			}else{
				$(form).find('#contact_location').removeClass('invalid');
				html = "location=" + location;
			}

			if(email === ""){
				$(form).find('#contact_email').addClass('invalid');
				error = true;
			}else if(re.test(email) === false){
				$(form).find('#contact_email').addClass('invalid');
				error = true;
			}else{
				$(form).find('#contact_email').removeClass('invalid');
				html += "&email="+ email;
			}
			
			if(subject === ""){
			//	$(form).find('#contact_subject').addClass('invalid');
				//error = true;
			}else{
				$(form).find('#contact_subject').removeClass('invalid');
				html += "&subject=" + subject;
			}
			
			if(message === ""){
				$(form).find('#contact_message').addClass('invalid');
				error = true;
			}else{
				$(form).find('#contact_message').removeClass('invalid');
				html += "&message="+ message;
			}
			
			var showError = function(){
				var iClass = $(form).find('.contact_send i').attr("class");

				$(form).find('.contact_send i').removeClass(iClass).addClass('icon-remove').delay(1500).queue(function(next){
					$(this).removeClass('icon-remove').addClass(iClass);
					next();
				});
				$(form).find('.contact_send').addClass('btn-danger').delay(1500).queue(function(next){
					$(this).removeClass('btn-danger');
					next();
				});
			};
			
			if(!error && !$tis.sendingMail) {
				$tis.sendingMail = true;
				$(form).find('.contact_send i').addClass('icon-cog icon-spin');
				$(form).find('.contact_send').addClass('disabled');

				$.ajax({
					type: 'POST',
					url: 'api/mail/contact',
					data: html,
					success: function(msg){
						$(form).find('.contact_send i').removeClass('icon-cog icon-spin');
						$(form).find('.contact_send').removeClass('disabled');
						
						if (msg === 'ok'){
							var iClass = $(form).find('.contact_send i').attr("class");

							$(form).find('.contact_send i').removeClass(iClass).addClass('icon-ok').delay(1500).queue(function(next){
								$(this).removeClass('icon-ok').addClass(iClass);
								next();
							});
							$(form).find('.contact_send').addClass('btn-success').delay(1500).queue(function(next){
								$(this).removeClass('btn-success');
								next();
							});
							$(form)[0].reset();
						}else{
							showError();
						}
						
						$tis.sendingMail = false;
					},
					error: function(){
						$(form).find('.contact_send i').removeClass('icon-cog icon-spin');
						$(form).find('.contact_send').removeClass('disabled');
						
						showError();
						$tis.sendingMail = false;
					}
				});
			} else{
				showError();
			}
			
			
			
			return false;
		});
	},
	
	buttons: function(){
		"use strict";
		var $tis = this;
		
		// Capture Other News 'Load More' Button click event.
		$("#load-more-btn").click(function(){
			$('#other-news li.disabled').css({display:'inline-block'});
			setTimeout(function() {
				$('#other-news li').removeClass("disabled");
			}, 200);
			$(this).hide(300);
		});
		
		// Capture Other News 'Load More' Button click event.
		$("#other-news li").click(function(){
			$(".page-overlay .progress").css({top: $(window).scrollTop() + $(window).height()/2 + 'px'});
			$(".page-overlay .loading, .page-overlay .progress").show(100);
			var newsDetails = $(this).data('news-details');
			if ( newsDetails !== undefined ){
				$tis.populateNews(newsDetails);
			}
		});
	}
};

Beat.init();

var initPhotoSwipeFromDOM = function(gallerySelector) {

	// parse slide data (url, title, size ...) from DOM elements
	// (children of gallerySelector)
	var parseThumbnailElements = function(el) {
		var thumbElements = el.childNodes,
			numNodes = thumbElements.length,
			items = [],
			figureEl,
			linkEl,
			size,
			item;

		for(var i = 0; i < numNodes; i++) {

			figureEl = thumbElements[i]; // <figure> element

			// include only element nodes
			if(figureEl.nodeType !== 1) {
				continue;
			}

			linkEl = figureEl.children[0]; // <a> element

			size = linkEl.getAttribute('data-size').split('x');

			// create slide object
			item = {
				src: linkEl.getAttribute('href'),
				w: parseInt(size[0], 10),
				h: parseInt(size[1], 10)
			};



			if(figureEl.children.length > 1) {
				// <figcaption> content
				item.title = figureEl.children[1].innerHTML;
			}

			if(linkEl.children.length > 0) {
				// <img> thumbnail element, retrieving thumbnail url
				item.msrc = linkEl.children[0].getAttribute('src');
			}

			item.el = figureEl; // save link to element for getThumbBoundsFn
			items.push(item);
		}

		return items;
	};

	// find nearest parent element
	var closest = function closest(el, fn) {
		return el && ( fn(el) ? el : closest(el.parentNode, fn) );
	};

	// triggers when user clicks on thumbnail
	var onThumbnailsClick = function(e) {
		e = e || window.event;
		e.preventDefault ? e.preventDefault() : e.returnValue = false;

		var eTarget = e.target || e.srcElement;

		// find root element of slide
		var clickedListItem = closest(eTarget, function(el) {
			return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
		});

		if(!clickedListItem) {
			return;
		}

		// find index of clicked item by looping through all child nodes
		// alternatively, you may define index via data- attribute
		var clickedGallery = clickedListItem.parentNode,
			childNodes = clickedListItem.parentNode.childNodes,
			numChildNodes = childNodes.length,
			nodeIndex = 0,
			index;

		for (var i = 0; i < numChildNodes; i++) {
			if(childNodes[i].nodeType !== 1) {
				continue;
			}

			if(childNodes[i] === clickedListItem) {
				index = nodeIndex;
				break;
			}
			nodeIndex++;
		}



		if(index >= 0) {
			// open PhotoSwipe if valid index found
			openPhotoSwipe( index, clickedGallery );
		}
		return false;
	};

	// parse picture index and gallery index from URL (#&pid=1&gid=2)
	var photoswipeParseHash = function() {
		var hash = window.location.hash.substring(1),
			params = {};

		if(hash.length < 5) {
			return params;
		}

		var vars = hash.split('&');
		for (var i = 0; i < vars.length; i++) {
			if(!vars[i]) {
				continue;
			}
			var pair = vars[i].split('=');
			if(pair.length < 2) {
				continue;
			}
			params[pair[0]] = pair[1];
		}

		if(params.gid) {
			params.gid = parseInt(params.gid, 10);
		}

		return params;
	};

	var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
		var pswpElement = document.querySelectorAll('.pswp')[0],
			gallery,
			options,
			items;

		items = parseThumbnailElements(galleryElement);

		// define options (if needed)
		options = {

			// define gallery index (for URL)
			galleryUID: galleryElement.getAttribute('data-pswp-uid'),

			getThumbBoundsFn: function(index) {
				// See Options -> getThumbBoundsFn section of documentation for more info
				var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
					pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
					rect = thumbnail.getBoundingClientRect();

				return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
			}

		};

		// PhotoSwipe opened from URL
		if(fromURL) {
			if(options.galleryPIDs) {
				// parse real index when custom PIDs are used
				// http://photoswipe.com/documentation/faq.html#custom-pid-in-url
				for(var j = 0; j < items.length; j++) {
					if(items[j].pid == index) {
						options.index = j;
						break;
					}
				}
			} else {
				// in URL indexes start from 1
				options.index = parseInt(index, 10) - 1;
			}
		} else {
			options.index = parseInt(index, 10);
		}

		// exit if index not found
		if( isNaN(options.index) ) {
			return;
		}

		if(disableAnimation) {
			options.showAnimationDuration = 0;
		}

		// Pass data to PhotoSwipe and initialize it
		gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
		gallery.init();
	};

	// loop through all gallery elements and bind events
	var galleryElements = document.querySelectorAll( gallerySelector );

	for(var i = 0, l = galleryElements.length; i < l; i++) {
		galleryElements[i].setAttribute('data-pswp-uid', i+1);
		galleryElements[i].onclick = onThumbnailsClick;
	}

	// Parse URL and open gallery if it contains #&pid=3&gid=1
	var hashData = photoswipeParseHash();
	if(hashData.pid && hashData.gid) {
		openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
	}
};

initPhotoSwipeFromDOM('.my-gallery');
