var myPlaylist = [
	{
		mp3:'music/ironija.mp3',
		oga:'',
		title:'Ironija',
		artist:'Creative band',
		rating:5,
		duration:'3:51',
		cover:'img/albums/ironija305.jpg'
	},
	{
		mp3:'music/probaj_me.mp3',
		oga:'',
		title:'Probaj me',
		artist:'Creative band',
		rating:4,
		duration:'3:50',
		cover:'music/album-cover.jpg'
	},
	{
		mp3:'music/bato.mp3',
		oga:'',
		title:'Marija ( Creative band ) - Bato',
		artist:'Creative band',
		rating:4,
		duration:'3:12',
		cover:'music/album-cover.jpg'
	},

];