$(document).ready(function() {

    var jcrop_api;

    $('#modal_crop_image').on('shown.bs.modal', function (event) {

        clearCoords();

        if (jcrop_api) {
            jcrop_api.release();
        }
    })

    $('.fileinput').on('change.bs.fileinput', function() {

        var target = $('.fileinput-preview > img'),
            imgData = target.attr('src'),
            img = new Image();

        target.addClass('img-responsive');

        img.onload = function(){
            $(target).Jcrop({
                aspectRatio: 1,
                onSelect: updateCoords,
                onRelease: clearCoords,
                trueSize: [img.width, img.height]
            }, function () {
                jcrop_api = this;
            });

            $('#submit').removeClass('disabled');
        };
        img.src = imgData;

    });

    function updateCoords(c){
        $('#img_x').val(c.x);
        $('#img_y').val(c.y);
        $('#img_w').val(c.w);
        $('#img_h').val(c.h);
    }

    function clearCoords()
    {
        $('[id^=img_]').val('');
    };

    function clearSelection() {

        if (jcrop_api) {
            jcrop_api.release();
        }

        $('#error_msg').closest('.form-group').removeClass('has-error').find('span#image-error').remove();
    }

    $('.fileinput').on('clear.bs.fileinput', function(){
        clearSelection();

        $('#submit').addClass('disabled');
    });

    $('#frmUploadImage').validate({
        rules: {
            image: {
                required: true
            }
        },
        highlight: function (element, errorClass) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            error.appendTo($('#error_msg'));
        }
    });

    function checkCropSelection() {
        var result = (parseInt($('#img_w').val()) > 0);

        if (!result) {
            $('#error_msg')
                .append('<span class="help-block">Please select a crop region then press upload.</span>')
                .closest('.form-group').addClass('has-error');
        }

        return result;
    }

    $('#submit').on('click', function(e) {

        e.preventDefault;

        var form = $('#frmUploadImage'),
            validator = form.validate();

        if (validator.form() && checkCropSelection()) {
            form.submit();
        } else {
            return false;
        }
    })
})
