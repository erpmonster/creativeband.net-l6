$(function () {

    $('#playlistsForm').on('shown.bs.modal', function (event) {
        var sender = $(event.relatedTarget),
            id = sender.data('id');

        $('#frmAddPlaylist :input').val('');

        if (id !== undefined) {
            $('#id').val(id);

            $.ajax({
                url: urlTo('admin/playlists/api/' + id),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(function(data){
                $('#title').val(data.title);
                $('#artist').val(data.artist);
                $('#duration').val(data.duration);
                $('#order_index').val(data.order_index);
                $('#active').val(data.active);
            })
        } else {
            $('#id').val('');
        }
    });

    $('#frmAddPlaylist').validate({
        rules: {
            title: {
                required: true,
                rangelength: [2,100]
            },
            artist: {
                required: true,
                rangelength: [2,100]
            },
            order_index: {
                required: true,
                number: true
            },
            active: {
                required: true
            },
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#submit').on('click', function(e) {

        e.preventDefault;

        var form = $('#frmAddPlaylist'),
            validator = form.validate();

        if (validator.form()) {
            form.submit();
        }
    })

})
