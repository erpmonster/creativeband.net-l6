$(function() {

    $('#selAlbum').on('change', function() {

        var id = $(this).val();

        location.href = urlTo('admin/albumsongs/' + id);

    });

    $('#albumsongsForm').on('shown.bs.modal', function (event) {
        var sender = $(event.relatedTarget),
            id = sender.data('id');

        $('#frmAddSong :input:not([type=hidden])').val('');

        if (id !== undefined) {
            $('#song_id').val(id);
            $('.modal-title').html('Edit song');

            $.ajax({
                type: "GET",
                url: urlTo('admin/albumsongs/api/' + id),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(function(data){
                $('#order_index').val(data.order_index);
                $('#title').val(data.title);
                $('#duration').val(data.duration);
                $('#subtitle').val(data.subtitle);
            })
        } else {
            $('#song_id').val('');
            $('.modal-title').html('Add song');
        }
    });


    $('#frmAddSong').validate({
        rules: {
            order_index: {
                required: true,
                number: true
            },
            title: {
                required: true,
                rangelength: [2,100]
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize(),
                album_id = $('#album_id').val();

            $.ajax({
                type: "POST",
                url: urlTo('admin/albumsongs/' + album_id),
                data: formData,
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(function (response) {
                if (!response.error) {
                    $('#membersForm').modal('hide');
                    location.reload();
                } else {
                    console.log('Došlo je do greške! Podaci nisu sačuvani.');
                }
            }).fail(function () {
                console.log('Došlo je do greške! Podaci nisu sačuvani.');
            });
        }
    });


})
