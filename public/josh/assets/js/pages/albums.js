$(function () {

    var imgWidth = 0,
        imgHeight = 0;

    if ($('.fileinput-preview > img').length != 0) {
        $('.fileinput').removeClass('fileinput-new').addClass('fileinput-exists');
    }

    $('.fileinput').on('change.bs.fileinput', function(e){
        var target = $('.fileinput-preview > img'),
            imgData = target.attr('src'),
            img = new Image();

            img.onload = function(){
                imgWidth = img.width;
                imgHeight = img.height;

                console.log(imgWidth);
                console.log(imgHeight);
            }
            img.src = imgData;
    });

    $('.fileinput').on('clear.bs.fileinput', function(){
        $('#error_msg').closest('.form-group').removeClass('has-error').find('span#image-error').remove();
    })

    $('#albumsForm').on('shown.bs.modal', function (event) {
        var sender = $(event.relatedTarget),
            id = sender.data('id');

        $('#frmAddAlbum :input').val('');

        if (id !== undefined) {
            $('#album_id').val(id);
            $('.modal-title').html('Edit album');

            $.ajax({
                url: urlTo('admin/albums/api/' + id),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(function(data){
                $('#title').val(data.title);
                $('#year').val(data.year);
                $('#description').val(data.description);
                $('#description_2').val(data.description_2);

                if (data.image != null ) {
                    var album_image = $('<img />').attr('src', urlTo(data.image));
                    $('.fileinput-preview').html(album_image);
                    $('.fileinput').removeClass('fileinput-new').addClass('fileinput-exists');
                }
            })
        } else {
            $('#album_id').val('');
            $('.modal-title').html('Add album');
        }
    });

    jQuery.validator.addMethod("isImageOversize", function(value, element) {
        if (imgWidth !=0 && imgHeight != 0) {
            if (imgWidth > 317 || imgHeight > 317) {
                var result = true;
            }
        }

        return this.optional(element) || (!result);
    }, "Image dimensions are greater than 317x317 px!");

    $('#frmAddAlbum').validate({
        rules: {
            title: {
                required: true,
                rangelength: [2,100]
            },
            year: {
                required: true,
                number: true
            },
            image: {
                isImageOversize: true
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'image') {
                error.appendTo($('#error_msg'));
            } else {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });

    $('#submit').on('click', function(e) {

        e.preventDefault;

        var form = $('#frmAddAlbum'),
            validator = form.validate();

        if (validator.form()) {
            form.submit();
        }
    })

})