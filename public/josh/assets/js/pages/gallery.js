$(document).ready(function() {

    if ($('#galleryCaption > span').text() != '') {
        $('#galleryCaption > a').removeClass('disabled');
    }

    $('#pnlUpload').removeClass('hidden');

    $('#galleryname').keyup(function(){

        if (this.value.length != 0) {
            $("#btnNewGallery").removeClass('disabled');
        } else {
            $("#btnNewGallery").addClass('disabled');
        }

    })

    /* new gallery */
    $('#btnNewGallery').on('click', function() {
        var gname = $('#galleryname').val();

        if (gname != '') {

            $.ajax({
                type: "GET",
                url: urlTo('admin/galleries/create/' + encodeURIComponent(gname)),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(function (data) {
                console.log(data);

                if (data != '0') {
                  window.location.href = urlTo('admin/galleries/' + data);
                }
            })
        }
    });

    /* Select gallery */
    $('[id^=filter-]').on('click', function(e) {
        var btn = $(e.target),
            id = btn.attr('data-id');

        window.location.href = urlTo('admin/galleries/' + id);
    });


    /* edit order_index */

    var getOrderIndexResponse = function(elem) {
        return function(data) {
            elem.prop('disabled', false);

            console.log(data);
        };
    };

    $('[id^=order_index-]').keyup(function(e){

        var order_index = this.value,
            id = $(this).data('id');

        if (order_index.length != 0 && $.isNumeric(order_index) && e.keyCode == 13) {
            $(this).prop('disabled', true);

            $.ajax({
                type: "GET",
                url: urlTo('admin/galleries/edit/order_index/' + id + '/' + order_index),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(getOrderIndexResponse($(this)));
        }
    })


    /* edit gallery activation */

    var getActiveResponse = function(elem, value) {
        return function(data) {
            elem.prop('disabled', false);
            switchActiveIcon(elem, value);
            elem.attr('data-active', value);

            console.log(data);
        };
    };


    $('[id^=btnActivate-]').on('click', function(e) {

        var btn = $(e.target),
            id = btn.attr('data-id'),
            value = btn.attr('data-active'),
            newvalue;

        $(this).prop('disabled', true);

        newvalue = (value != 0) ? 0:1;

        $.ajax({
            type: "GET",
            url: urlTo('admin/galleries/edit/active/' + id + '/' + newvalue),
            beforeSend: function() {
                $('.cbloader').show();
            },
            complete: function(){
                $('.cbloader').hide();
            }
        }).done(getActiveResponse($(this), newvalue));

    });

    function switchActiveIcon(elem, value) {

        if (value == 0) {
            elem.children('i').removeClass('fa-check').addClass('fa-times-circle');
        } else {
            elem.children('i').removeClass('fa-times-circle').addClass('fa-check');
        }
    }


    /* delete image */

    $('[id^=btnDeleteImage-]').on('click', function(e) {
        var btn = $(e.target),
            id = btn.attr('data-id');

        $.ajax({
            type: "GET",
            url: urlTo('admin/deleteimage/galleries/' + id),
            beforeSend: function() {
                $('.cbloader').show();
            },
            complete: function(){
                $('.cbloader').hide();
            }
        }).done(function (data) {
            console.log(data);
            window.location.href = urlTo('admin/galleries/' + $('#gallery_id').val());

        })
    })


    /* edit image activation */

    var getImageActiveResponse = function(elem, value) {
        return function(data) {
            elem.prop('disabled', false);
            switchActiveIcon(elem, value);
            elem.attr('data-active', value);

            console.log(data);
        };
    };


    $('[id^=btnImageActivate-]').on('click', function(e) {

        var btn = $(e.target),
            id = btn.attr('data-id'),
            value = btn.attr('data-active'),
            newvalue;

        $(this).prop('disabled', true);

        newvalue = (value != 0) ? 0:1;

        $.ajax({
            type: "GET",
            url: urlTo('admin/galleries/edit/image/active/' + id + '/' + newvalue),
            beforeSend: function() {
                $('.cbloader').show();
            },
            complete: function(){
                $('.cbloader').hide();
            }
        }).done(getImageActiveResponse($(this), newvalue));

    });


    /* edit image order_index */

    var getImageOrderIndexResponse = function(elem) {
        return function(data) {
            elem.prop('disabled', false);

            console.log(data);
        };
    };

    $('[id^=gimg_order_index-]').keyup(function(e){

        var order_index = this.value,
            id = $(this).data('id');

        if (order_index.length != 0 && $.isNumeric(order_index) && e.keyCode == 13) {
            $(this).prop('disabled', true);

            $.ajax({
                type: "GET",
                url: urlTo('admin/galleries/edit/image/order_index/' + id + '/' + order_index),
                beforeSend: function() {
                    $('.cbloader').show();
                },
                complete: function(){
                    $('.cbloader').hide();
                }
            }).done(getImageOrderIndexResponse($(this)));
        }
    })

})
