$(function () {

    $('#frmBand').validate({
        rules: {
            name: {
                required: true,
                rangelength: [2,250]
            },
            description: {
                rangelength: [4,2000]
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#submit').on('click', function(e) {

        e.preventDefault;

        var form = $('#frmBand'),
            validator = form.validate();

        if (validator.form()) {
            form.submit();
        }
    })
});