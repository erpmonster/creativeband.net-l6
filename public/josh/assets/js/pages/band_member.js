$(function () {

    var imgWidth = 0,
        imgHeight = 0;

    if ($('.fileinput-preview > img').length != 0) {
            $('.fileinput').removeClass('fileinput-new').addClass('fileinput-exists');
    }

    $('.fileinput').on('change.bs.fileinput', function(e){

       setTimeout(function() {
           imgWidth = $('.fileinput-preview').css('width').slice(0,-2);
           imgHeight = $('.fileinput-preview').css('height').slice(0,-2);

           /*slika je za 10px manja*/
           imgWidth = imgWidth - 10;
           imgHeight = imgHeight - 10;

           console.log(imgWidth);
           console.log(imgHeight);
       }, 250);
    });

    $('.fileinput').on('clear.bs.fileinput', function(){
        $('#error_msg').closest('.form-group').removeClass('has-error').find('span#image-error').remove();
    })

    jQuery.validator.addMethod("isImageOversize", function(value, element) {
        if (imgWidth !=0 && imgHeight != 0) {
            if (imgWidth > 372 || imgHeight > 371) {
                var result = true;
            }
        }

        return this.optional(element) || (!result);
    }, "Image dimensions are greater than 372x371 px!");

    $('#frmBandMember').validate({
        rules: {
            first_name: {
                required: true,
                rangelength: [2,250]
            },
            last_name: {
                required: true,
                rangelength: [2,250]
            },
            role: {
                required: true,
                rangelength: [2,64]
            },
            description: {
                required: true
            },
            order_index: {
                required: true,
                number: true
            },
            image: {
                isImageOversize: true
            }
        },
        highlight: function (element, errorClass) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'image') {
                error.appendTo($('#error_msg'));
            } else {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });

    $('#submit').on('click', function(e) {

        e.preventDefault;

        var form = $('#frmBandMember'),
            validator = form.validate();

        if (validator.form()) {
            form.submit();
        }
    })






})