$(document).ready(function() {

    if ($('.fileinput-preview > img').length != 0) {
        $('.fileinput').removeClass('fileinput-new').addClass('fileinput-exists');
    }

    $('.fileinput').on('change.bs.fileinput', function(){
        var img_data = $('.fileinput-preview > img').prop('src'),
            domain = $('#domain').data('domain'),
            domain_id = $('#domain').data('domain_id');

        $.ajax({
            type: "POST",
            url: urlTo('admin/addb64image/' + domain + '/' + domain_id),
            data: img_data
        }).done(function () {
            console.log('ok');
        })
    });

    $('.fileinput').on('clear.bs.fileinput', function(){
        var domain = $('#domain').data('domain'),
            domain_id = $('#domain').data('domain_id');

        $.ajax({
            type: "GET",
            url: urlTo('admin/deleteimage/' + domain + '/' + domain_id),
        }).done(function () {
            console.log('ok');
        })
    })

});